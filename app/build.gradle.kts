plugins {
    id(BuildPlugins.androidApplication)
    id(BuildPlugins.kotlinAndroidExtensions)
    id("kotlin-android")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    kotlin("kapt")
}


android {
    compileSdkVersion(AndroidSdk.compileSdk)
    buildToolsVersion(AndroidSdk.buildTools)

    defaultConfig {
        applicationId = "com.u4u.universityforuskt"
        minSdkVersion(AndroidSdk.minSdk)
        targetSdkVersion(AndroidSdk.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {

    // Support Libraries
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(BuildPlugins.kotlinStdLib)
    implementation(SupportLibraries.appCompat)
    implementation(SupportLibraries.ktxCore)
    implementation(SupportLibraries.legacy)

    // UI Libraries
    implementation(UILibraries.expandable)
    implementation(UILibraries.constraintLayout)
    implementation(UILibraries.recyclerView)
    implementation(UILibraries.cardView)
    implementation(UILibraries.toasty)
    implementation(UILibraries.materialDesign)

    // Architecture Components Libraries
    implementation(ArchComponentsLibraries.viewmodel)
    implementation(ArchComponentsLibraries.livedata)
    implementation(ArchComponentsLibraries.navigationFragment)
    implementation(ArchComponentsLibraries.navigationUI)
    implementation(ArchComponentsLibraries.palette)
    implementation(ArchComponentsLibraries.workManager)
    implementation(ArchComponentsLibraries.roomRuntime)
    implementation("androidx.legacy:legacy-support-v4:1.0.0")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0")
    implementation("androidx.coordinatorlayout:coordinatorlayout:1.1.0")
    kapt(ArchComponentsLibraries.roomCompiler)
    implementation(ArchComponentsLibraries.kts)

    // Network Libraries
    implementation(NetworkLibraries.retrofit)
    implementation(NetworkLibraries.retrofitGson)
    implementation(NetworkLibraries.okhttp)
    implementation(NetworkLibraries.loggingInterceptor)
    implementation(NetworkLibraries.gson)
    debugImplementation(NetworkLibraries.debugDatabase)

    // DI Libraries
    implementation(DILibraries.koinViewmodel)
    implementation(DILibraries.koinScope)
    implementation(DILibraries.koinFragment)

    // Utils Libraries
    implementation(UtilsLibraries.leakCanary)
    implementation(UtilsLibraries.androidWeek)
    implementation(UtilsLibraries.picasso)
    implementation(UtilsLibraries.picassoDownloader)
    implementation(UtilsLibraries.floatingButton)
    implementation(UtilsLibraries.gifDrawable)

    //firebase
    implementation(FirebaseLibraries.firebaseAnalytics)
    implementation(FirebaseLibraries.firebaseMessaging)
    implementation(FirebaseLibraries.firebaseIid)
    implementation(FirebaseLibraries.firebaseCrashlytics)

    // Test Libraries
    testImplementation(TestLibraries.junit4)
    testImplementation(TestLibraries.extJunit)
    androidTestImplementation(TestLibraries.espresso)
}
