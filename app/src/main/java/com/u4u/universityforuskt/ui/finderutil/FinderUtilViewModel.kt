package com.u4u.universityforuskt.ui.finderutil

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.models.Comment
import com.u4u.universityforuskt.data.models.CommentRequest
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.repository.FinderUtil.FinderUtilRepositoryImpl
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.launch
import java.util.*

@Suppress("UNCHECKED_CAST")
class FinderUtilViewModel(
    private val repository: FinderUtilRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponseSubjects: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSubjects: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSubjects

    private val _dataResponseNewComment: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseNewComment: LiveData<Pair<Boolean, Any?>> get() = _dataResponseNewComment

    fun getTeacherSubjects(teacher_id: Int) {
        viewModelScope.launch {
            val response = repository.getTeacherSubjects(Singleton.token, teacher_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data1 = response.data as List<Data>
                    if (data1.isEmpty()) {
                        _dataResponseNewComment.value = Pair(false, R.string.SubjectError)
                    } else {
                        val data = Gson().toJson(data1)
                        _dataResponseNewComment.value = Pair(true, data)
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseNewComment.value = Pair(false, R.string.SubjectError)
                }
            }
        }
    }

    fun addNewComment(
        amenity_qualification: Double,
        classes_qualification: Double,
        description: String,
        experience_qualification: Double,
        explication_qualification: Double,
        final_note: Double,
        semester: String,
        student_qualification: Double,
        subject: Int,
        teacher: Int
    ) {
        viewModelScope.launch {
            val commentRequest = CommentRequest(
                amenity_qualification,
                classes_qualification,
                description,
                experience_qualification,
                explication_qualification,
                final_note,
                semester,
                Singleton.user_id,
                student_qualification,
                subject,
                teacher,
                Calendar.YEAR
            )
            val response = repository.addNewComment(Singleton.token, commentRequest)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as Comment
                    _dataResponseNewComment.value = Pair(true, data)
                }
                Result.Status.ERROR -> {
                    _dataResponseNewComment.value = Pair(false, R.string.AddCommentError)
                }
            }
        }
    }

}