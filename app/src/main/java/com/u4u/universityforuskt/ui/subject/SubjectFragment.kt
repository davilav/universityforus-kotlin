package com.u4u.universityforuskt.ui.subject

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.NotesRequest
import com.u4u.universityforuskt.ui.adapters.NotesAdapter
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.android.synthetic.main.subject_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("UNCHECKED_CAST", "NAME_SHADOWING")
class SubjectFragment : Fragment() {
    private val viewModel: SubjectViewModel by viewModel()
    private lateinit var adapter: NotesAdapter
    private var subjectId: Int? = null
    private var itemTouchHelper: ItemTouchHelper? = null
    private var data: MutableList<NotesRequest>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.subject_fragment, container, false)
    }

    private fun setOnClickListener() {
        subjectFloatingActionMenu.setOnClickListener {
            if (subjectId != null) {
                viewModel.addNotes(subjectId as Int)
            }
        }
        subjectSearchViewFinderTxt.setOnClickListener { findNavController().navigate(R.id.action_subjectFragment_to_finderFragment) }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        setOnClickListener()
        super.onActivityCreated(savedInstanceState)
        viewModel.wakeUp()
        if (!Singleton.isOnline) {
            subjectSearchViewFinderTxt.visibility = View.GONE
            subjectFloatingActionMenu.visibility = View.GONE
        }
        subjectId = arguments?.getInt("subject_id")
        if (subjectId != null) {
            viewModel.getNotes(subjectId as Int)
        }

        subjectRecyclerViewSubjects.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        viewModel.dataResponseGetNotes.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    data = response.second as MutableList<NotesRequest>
                    adapter = NotesAdapter(data!!)
                    subjectRecyclerViewSubjects.adapter = adapter
                    adapter.notifyDataSetChanged()
                    if (Singleton.isOnline) {
                        itemTouchHelper = ItemTouchHelper(
                            viewModel.SwipeToDelete(
                                adapter
                            )
                        )
                        itemTouchHelper!!.attachToRecyclerView(subjectRecyclerViewSubjects)
                    }
                    adapter.userLiveData.observe(viewLifecycleOwner, Observer { response ->
                        viewModel.setNotes(
                            response.toString().toInt(),
                            adapter,
                            subjectRecyclerViewSubjects
                        )


                    })
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseSetNotes.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseAddNotes.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    data!!.add(response.second as NotesRequest)
                    adapter.notifyDataSetChanged()
                }
                false -> {
                }
            }
        })


        viewModel.dataResponseDeleteNote.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                }
                false -> {
                    Snackbar.make(requireView(), response.second.toString(), Snackbar.LENGTH_LONG)
                        .show()
                    adapter.notifyDataSetChanged()
                }

            }
        })

    }

    override fun onPause() {
        super.onPause()
        subjectRelativeLayoutNotes.requestFocus()
    }

}
