package com.u4u.universityforuskt.ui.adapters

import android.content.ContentValues
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.u4u.universityforuskt.data.models.NotesRequest
import kotlinx.android.synthetic.main.layout_notes.view.*
import org.koin.ext.isFloat
import org.koin.ext.isInt


class NotesAdapter(
    var academicResponses: MutableList<NotesRequest>
) : RecyclerView.Adapter<NotesAdapter.ViewHolderData>(), View.OnClickListener {
    private var removedPosition: Int = 0
    private var removedItem: NotesRequest? = null
    private val parenView: ViewGroup? = null
    var isRemoved: Boolean = true
    private val _userLiveData: MutableLiveData<Any?> = MutableLiveData()
    val userLiveData: LiveData<Any?> get() = _userLiveData

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(com.u4u.universityforuskt.R.layout.layout_notes, parenView, false)
        view.setOnClickListener(this)
        return ViewHolderData(view)
    }



    override fun onBindViewHolder(@NonNull holder: ViewHolderData, position: Int) {
        holder.putData(academicResponses[position])
    }

    override fun getItemCount(): Int {
        return academicResponses.size
    }

    fun deleteItem(viewholder:RecyclerView.ViewHolder,position: Int){
        removedPosition = position
        removedItem = academicResponses[position]

        academicResponses.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
        isRemoved = true
        Snackbar.make(viewholder.itemView,"$removedItem deleted",Snackbar.LENGTH_LONG).setAction("UNDO"){
            academicResponses.add(removedPosition,removedItem as NotesRequest)
            notifyItemInserted(removedPosition)
            notifyDataSetChanged()
            isRemoved = false
        }.show()
    }
    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private var description: EditText = itemView.layoutNotesName
        private var percentage: EditText = itemView.layoutNotesPercentage
        private var note: EditText = itemView.layoutNotesNote
        fun putData(data: NotesRequest) {
            description.setText(data.description)
            percentage.setText(data.percent.toString())
            note.setText(data.value.toString())

            description.onFocusChangeListener = View.OnFocusChangeListener { _, p1 ->
                if (!p1) {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (!description.text.isNullOrBlank()) {
                            val value: String = description.text.toString()
                            academicResponses[adapterPosition].description = value
                            try {
                                notifyItemChanged(adapterPosition)
                            } catch (e: Exception) {
                                Log.d(ContentValues.TAG, e.toString())
                            }
                            _userLiveData.value = adapterPosition
                        }
                    }
                }
            }

            percentage.onFocusChangeListener = View.OnFocusChangeListener { _, p1 ->
                if (!p1) {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (!percentage.text.isNullOrBlank() && percentage.text.toString()
                                .isInt()
                        ) {
                            val value: Int = percentage.text.toString().toInt()
                            academicResponses[adapterPosition].percent = value
                            try {
                                notifyItemChanged(adapterPosition)
                            } catch (e: Exception) {
                                Log.d(ContentValues.TAG, e.toString())
                            }
                            _userLiveData.value = adapterPosition
                        }
                    }

                }
            }

            note.onFocusChangeListener = View.OnFocusChangeListener { _, p1 ->
                if (!p1) {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (!note.text.isNullOrBlank() && note.text.toString().isFloat()) {
                            val value: Double = note.text.toString().toDouble()
                            academicResponses[adapterPosition].value = value
                            _userLiveData.value = adapterPosition
                            try {
                                notifyItemChanged(adapterPosition)
                            } catch (e: Exception) {
                                Log.d(ContentValues.TAG, e.toString())
                            }
                        }
                    }
                }
            }

        }

    }

    override fun onClick(p0: View?) {
    }


}