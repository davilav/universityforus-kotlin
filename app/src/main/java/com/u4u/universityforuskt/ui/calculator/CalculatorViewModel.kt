package com.u4u.universityforuskt.ui.calculator


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.repository.subjects.SubjectRepositoryImpl
import com.u4u.universityforuskt.ui.adapters.NotesCalculatorAdapter

@Suppress("UNCHECKED_CAST")
class CalculatorViewModel(
    private val repository: SubjectRepositoryImpl,
    private val databaseImpl: LocalDataBase

) : ViewModel() {
    private val _dataResponseDeleteNote: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseDeleteNote: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteNote


    inner class SwipeToDelete(val adapter: NotesCalculatorAdapter) :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            TODO("Not yet implemented")
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

            if (adapter.academicResponses.size <= 1) {
                _dataResponseDeleteNote.value = Pair(false, "No se puede eliminar esta nota")
                adapter.isUnique = true
                val pos = viewHolder.adapterPosition
                adapter.deleteItem(viewHolder, pos)
                adapter.reset()
            } else {
                adapter.isUnique = false
                val pos = viewHolder.adapterPosition
                adapter.deleteItem(viewHolder, pos)
            }

        }
    }

}