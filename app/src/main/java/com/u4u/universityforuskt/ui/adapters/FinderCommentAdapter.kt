package com.u4u.universityforuskt.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.data.models.Comment
import kotlinx.android.synthetic.main.layout_comment.view.*
import java.lang.String


class FinderCommentAdapter(
    private val comments:List<Comment>
): RecyclerView.Adapter<FinderCommentAdapter.ViewHolderData>() {

    private val parentView: ViewGroup? = null

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(com.u4u.universityforuskt.R.layout.layout_comment, parentView, false)
        return ViewHolderData(view)
    }

    override fun onBindViewHolder(
        @NonNull holder: ViewHolderData,
        position: Int
    ) {
        holder.putData(comments[position])
    }

    override fun getItemCount(): Int {
        return comments.size
    }


    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun putData(comments: Comment) {
            itemView.textViewLayoutSubjectFinder.text=comments.name
            val x = "Nota final: " + String.valueOf(comments.final_note)
            itemView.textViewLayoutNoteFinder.text = x
            itemView.textViewLayoutCommentFinder.text=comments.description
        }

    }

}