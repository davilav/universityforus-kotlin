package com.u4u.universityforuskt.ui.adapters

import android.content.ContentValues.TAG
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.u4u.universityforuskt.data.models.NotesRequest
import kotlinx.android.synthetic.main.layout_notes.view.*
import org.koin.ext.isFloat
import org.koin.ext.isInt


class NotesCalculatorAdapter(
    var academicResponses: MutableList<NotesRequest>
) : RecyclerView.Adapter<NotesCalculatorAdapter.ViewHolderData>(), View.OnClickListener {
    var idle_min: Long = 1000 // 4 seconds after user stops typing
    var last_text_edit: Long = 0
    var isValueNecesary = true
    var isPercentNecesary = true
    var lastPercent: String = "0"
    var lastValue: String = "0"
    var h: Handler = Handler()
    private var removedPosition: Int = 0
    private var removedItem: NotesRequest? = null
    private val parenView: ViewGroup? = null
    private var mParenView: ViewGroup? = null
    var iconTxt = "%"
    var isUnique: Boolean = false
    private val _userLiveData: MutableLiveData<Any?> = MutableLiveData()
    val userLiveData: LiveData<Any?> get() = _userLiveData

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(com.u4u.universityforuskt.R.layout.layout_notes, parenView, false)
        view.setOnClickListener(this)
        mParenView = parent
        return ViewHolderData(view)
    }


    override fun onBindViewHolder(@NonNull holder: ViewHolderData, position: Int) {
        holder.putData(academicResponses[position])
    }

    override fun getItemCount(): Int {
        return academicResponses.size
    }

    fun deleteItem(viewholder: RecyclerView.ViewHolder, position: Int) {
        removedPosition = position
        removedItem = academicResponses[position]
        academicResponses.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
        _userLiveData.value = 0
        if (!isUnique) {
            Snackbar.make(viewholder.itemView, "$removedItem deleted", Snackbar.LENGTH_LONG)
                .setAction("UNDO") {
                    reset()
                }.show()
        }

    }

    fun reset() {
        _userLiveData.value = 0
        academicResponses.add(removedPosition, removedItem as NotesRequest)
        notifyItemInserted(removedPosition)
        notifyDataSetChanged()
    }

    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private var description: EditText = itemView.layoutNotesName
        private var percentage: EditText = itemView.layoutNotesPercentage
        private var note: EditText = itemView.layoutNotesNote
        private var icon: TextView = itemView.layoutNotesPercentageOnly

        fun putData(data: NotesRequest) {
            description.setText(data.description)
            percentage.setText(data.percent.toString())
            note.setText(data.value.toString())
            icon.text = iconTxt

            description.onFocusChangeListener = View.OnFocusChangeListener { _, p1 ->
                if (!p1) {
                    if (adapterPosition != RecyclerView.NO_POSITION) {
                        if (!description.text.isNullOrBlank()) {
                            val value: String = description.text.toString()
                            academicResponses[adapterPosition].description = value
                            try {
                                notifyItemChanged(adapterPosition)
                            } catch (e: Exception) {
                                Log.d(TAG, e.toString())
                            }
                        }
                    }

                }
            }

            percentage.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (percentage.text.toString() != lastPercent) {
                        isPercentNecesary = true
                    }
                    last_text_edit = System.currentTimeMillis()
                    h.postDelayed({
                        if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                            _userLiveData.value = 0
                            if (isPercentNecesary) {
                                isPercentNecesary = false
                                if (adapterPosition != RecyclerView.NO_POSITION) {
                                    if (!percentage.text.isNullOrBlank() && percentage.text.toString()
                                            .isInt()
                                    ) {
                                        val value: Int = percentage.text.toString().toInt()
                                        academicResponses[adapterPosition].percent = value
                                        lastPercent = percentage.text.toString()
                                        try {
                                            notifyItemChanged(adapterPosition)
                                        } catch (e: Exception) {
                                            Log.d(TAG, e.toString())
                                        }
                                        _userLiveData.value = value
                                        percentage.clearFocus()
                                    }
                                }
                            }
                        }
                    }, idle_min)
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

            })

            note.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (note.text.toString() != lastValue) {
                        isValueNecesary = true
                    }

                    last_text_edit = System.currentTimeMillis()
                    h.postDelayed({
                        if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                            _userLiveData.value = 0
                            if (isValueNecesary) {
                                isValueNecesary = false
                                if (adapterPosition != RecyclerView.NO_POSITION) {
                                    if (!note.text.isNullOrBlank() && note.text.toString()
                                            .isFloat()
                                    ) {
                                        val value: Double = note.text.toString().toDouble()
                                        lastValue = note.text.toString()
                                        academicResponses[adapterPosition].value = value
                                        try {
                                            notifyItemChanged(adapterPosition)
                                        } catch (e: Exception) {
                                            Log.d(TAG, e.toString())
                                        }
                                        _userLiveData.value = value
                                        note.clearFocus()
                                    }
                                }
                            }
                        }
                    }, idle_min)

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

            })

        }


    }

    override fun onClick(p0: View?) {
    }


}