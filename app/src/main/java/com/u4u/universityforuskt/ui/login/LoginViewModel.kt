package com.u4u.universityforuskt.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.local.db.student.Student
import com.u4u.universityforuskt.data.models.LoginRequest
import com.u4u.universityforuskt.data.repository.login.LoginRepositoryImpl
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.launch


class LoginViewModel(
    private val repository: LoginRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponse: MutableLiveData<Pair<Boolean, String?>> = MutableLiveData()
    val dataResponse: LiveData<Pair<Boolean, String?>> get() = _dataResponse

    private val _dataResponsePass: MutableLiveData<Pair<Boolean, String?>> = MutableLiveData()
    val dataResponsePass: LiveData<Pair<Boolean, String?>> get() = _dataResponsePass

    private val _dataResponseUser: MutableLiveData<Pair<Boolean, String?>> = MutableLiveData()
    val dataResponseUser: LiveData<Pair<Boolean, String?>> get() = _dataResponseUser


    fun validateData(username: String,password: String) {
        viewModelScope.launch {
            if (username.isNotEmpty()){
                if (password.isNotEmpty()){
                    val response = repository.setLoginUser(LoginRequest(password,username))
                    when (response.status) {
                        Result.Status.SUCCESS -> {
                            _dataResponse.value = Pair(true, null)
                            val studentDB = Student(
                                Singleton.user_id,
                                Singleton.token,
                                Singleton.first_name,
                                Singleton.last_name,
                                Singleton.username,
                                Singleton.career,
                                Singleton.faculty,
                                Singleton.email,
                                Singleton.university,
                                1,
                                "Career"
                            )
                            get(studentDB)
                        }
                        Result.Status.ERROR -> {
                            val dataResult = response.data as? String
                            _dataResponse.value = Pair(false, dataResult)
                        }
                    }
                } else {
                    _dataResponsePass.value = Pair(false, "Por favor ingresa un password")
                }
            } else {
                _dataResponseUser.value = Pair(false, "Por favor ingresa un usuario")
            }
        }
    }

    private suspend fun insert(student: Student) {
        databaseImpl.getStudentDao().insertStudent(student)
    }

    private suspend fun get(student: Student) {
        val data = databaseImpl.getStudentDao().getStudent()
        if (data.isEmpty()) {
            insert(student)
        } else {
            val studentDB = student.copy(isLogged = 1)
            databaseImpl.getStudentDao().updateStudent(studentDB)
        }
    }
}