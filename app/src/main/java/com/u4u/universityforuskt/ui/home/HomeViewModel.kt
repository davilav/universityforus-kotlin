package com.u4u.universityforuskt.ui.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.local.db.schedule.Schedule
import com.u4u.universityforuskt.data.models.*
import com.u4u.universityforuskt.data.repository.home.HomeRepositoryImpl
import com.u4u.universityforuskt.utils.Singleton
import com.u4u.universityforuskt.utils.Utils
import kotlinx.coroutines.launch
import java.util.*

@Suppress("UNCHECKED_CAST")
class HomeViewModel(
    private val repository: HomeRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {
    private val _dataResponseEvents: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseEvents: LiveData<Pair<Boolean, Any?>> get() = _dataResponseEvents

    private val _dataResponseSubjects: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSubjects: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSubjects

    private val _dataResponseGroups: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseGroups: LiveData<Pair<Boolean, Any?>> get() = _dataResponseGroups

    private val _dataResponseSetGroups: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSetGroups: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSetGroups

    private val _dataResponseSchedule: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSchedule: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSchedule

    private val _dataResponseUpdateStudent: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseUpdateStudent: LiveData<Pair<Boolean, Any?>> get() = _dataResponseUpdateStudent

    private val _dataResponseDeleteGroup: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseDeleteGroup: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteGroup

    private val _dataResponseNewStudent: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseNewStudent: LiveData<Pair<Boolean, Any?>> get() = _dataResponseNewStudent

    private val _dataResponseSetHistory: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSetHistory: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSetHistory

    private val _dataResponseCareer: MutableLiveData<Pair<Boolean, String?>> = MutableLiveData()
    val dataResponseCareer: LiveData<Pair<Boolean, String?>> get() = _dataResponseCareer

    private val _dataResponseSetNotes: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSetNotes: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSetNotes


    private suspend fun setOfflineEvents(events: List<Event>) {
        databaseImpl.getScheduleDao().nukeTable()
        for (event in events) {
            databaseImpl.getScheduleDao().insertEvents(
                Schedule(
                    0, event.id,
                    event.class_id,
                    event.title,
                    event.startTime,
                    event.endTime,
                    event.location,
                    event.subject_name,
                    event.color,
                    event.credits,
                    event.isAllDay,
                    event.isCanceled,
                    event.building_name,
                    event.classroom_Name
                )
            )
        }
    }

    private suspend fun isOfflineEventsEmpty(): List<Event>? {
        if (!databaseImpl.getScheduleDao().getEvents().isNullOrEmpty()) {
            val eventDB = databaseImpl.getScheduleDao().getEvents()
            val myEvents = mutableListOf<Event>()
            for (event in eventDB) {
                myEvents.add(
                    Event(
                        event.id as Long,
                        event.classId as Long,
                        event.title as String,
                        event.startTime as Calendar,
                        event.endTime as Calendar,
                        event.location as String,
                        event.subjectName as String,
                        event.color as Int,
                        event.credits as Int,
                        event.isAllDay as Boolean,
                        event.isCanceled as Boolean,
                        event.building_name,
                        event.classroom_Name
                    )
                )
            }
            return myEvents
        }
        return null
    }

    fun getEvents(view: View) {
        viewModelScope.launch {
            val isEmpty = isOfflineEventsEmpty()
            if (isEmpty != null && !Singleton.isOnline) {
                _dataResponseEvents.value = Pair(true, isEmpty as List<Nothing>)
            } else {
                val response = repository.getEvents(Singleton.token, Singleton.user_id)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        if (response.data != null) {
                            val events = mutableListOf<Event>()
                            val data = response.data as List<Data>
                            Singleton.events = data

                            for (i in data.indices) {
                                val init = Calendar.getInstance()
                                init.set(Calendar.HOUR_OF_DAY, data[i].time_start.toInt())
                                init.set(Calendar.MINUTE, 0)
                                init.set(Calendar.DAY_OF_MONTH, validateDay(data[i].day))
                                val end = Calendar.getInstance()
                                end.set(Calendar.HOUR_OF_DAY, data[i].time_end.toInt() - 1)
                                end.set(Calendar.MINUTE, 59)
                                end.set(Calendar.DAY_OF_MONTH, validateDay(data[i].day))
                                events.add(
                                    Event(
                                        data[i].group_id.toLong(),
                                        data[i].group_student.toLong(),
                                        data[i].subject_name,
                                        init,
                                        end,
                                        data[i].credits_number.toString(),
                                        data[i].subject_name,
                                        view.resources.getColor(
                                            R.color.u4u_blue
                                        ),
                                        data[i].credits_number,
                                        isAllDay = false,
                                        isCanceled = false,
                                        building_name = data[i].building_name,
                                        classroom_Name = data[i].classroom_Name
                                    )
                                )
                            }
                            setOfflineEvents(events as List<Event>)
                            _dataResponseEvents.value = Pair(true, events as List<Nothing>)
                        } else {
                            _dataResponseEvents.value = Pair(false, "error")
                        }
                    }
                    Result.Status.ERROR -> {
                        _dataResponseEvents.value = Pair(false, "error")
                    }
                }
            }
        }
    }

    private fun validateDay(mday: String): Int {
        val cal = Calendar.getInstance()
        val dayMonth = cal.get(Calendar.DAY_OF_MONTH)
        val dayWeek = cal.get(Calendar.DAY_OF_WEEK)
        when (mday.toUpperCase(Locale.ROOT).toLowerCase(Locale.ROOT)) {
            "lunes" -> return dayMonth + (2 - dayWeek)
            "martes" -> return dayMonth + (3 - dayWeek)
            "miercoles" -> return dayMonth + (4 - dayWeek)
            "jueves" -> return dayMonth + (5 - dayWeek)
            "viernes" -> return dayMonth + (6 - dayWeek)
            "sabado" -> return dayMonth + (7 - dayWeek)
        }
        return dayMonth + (2 - dayWeek)
    }

    fun getSubjects(careerId: Int) {
        viewModelScope.launch {
            val response = repository.getSubjects(Singleton.token, careerId)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as List<Data>
                    if (data.isEmpty()) {
                        _dataResponseSubjects.value = Pair(false, "no se encontraron materias")
                    } else {
                        _dataResponseSubjects.value = Pair(true, data)
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseSubjects.value = Pair(false, "no se encontraron materias")
                }
            }
        }
    }

    fun wakeUp() {
        viewModelScope.launch {
        }
    }

    fun getGroups(subjectId: Int, subject_name: String) {
        viewModelScope.launch {
            var repeated = false
            for (i in Singleton.events!!) {
                if (i.subject_name == subject_name) {
                    repeated = true
                    break
                }
            }
            if (!repeated) {
                val response = repository.getGroups(Singleton.token, subjectId)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        val data = filter(response.data as List<Data>)
                        if (data.isEmpty()) {
                            _dataResponseGroups.value = Pair(false, "no se encontraron grupos")
                        } else {
                            _dataResponseGroups.value = Pair(true, data)
                        }
                    }
                    Result.Status.ERROR -> {
                        _dataResponseGroups.value = Pair(false, "no se encontraron grupos")
                    }
                }
            } else {
                _dataResponseGroups.value = Pair(false, "ya estas inscrito a esta materia")
            }

        }
    }

    fun getSchedule(subjectId: Int, groupId: Int) {
        viewModelScope.launch {
            val response = repository.getSchedule(Singleton.token, subjectId, groupId)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as List<Data>
                    if (data.isEmpty()) {
                        _dataResponseSchedule.value = Pair(true, "no se encontraron horarios")
                    } else {
                        _dataResponseSchedule.value = Pair(true, data)
                    }
                    //TODO(not repeat)
                }
                Result.Status.ERROR -> {
                    _dataResponseSchedule.value = Pair(false, "no se encontraron horarios")
                }
            }
        }
    }

    fun setGroups(group: Int) {
        viewModelScope.launch {
            val groupRequest = Group(group, 1, Singleton.user_id)
            val response = repository.setGroups(Singleton.token, groupRequest)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseSetGroups.value = Pair(true, "Succes")
                }
                Result.Status.ERROR -> {
                    _dataResponseSetGroups.value = Pair(false, response.data)
                }
            }
        }

    }

    fun setNotes(group_id: Int) {
        viewModelScope.launch {
            val notesRequest = NotesRequest(0, "_-_-", group_id, 1, Singleton.user_id, 1.0)
            val response = repository.setNotes(Singleton.token, notesRequest)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseSetNotes.value = Pair(true, "Succes")
                }
                Result.Status.ERROR -> {
                    _dataResponseSetNotes.value = Pair(false, response.data)
                }
            }
        }

    }

    fun deleteGroup(note_id: Int) {
        viewModelScope.launch {
            val response = repository.deleteSubject(Singleton.token, note_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseDeleteGroup.value = Pair(true, "Succes")
                    //TODO(not repeat)
                }
                Result.Status.ERROR -> {
                    _dataResponseDeleteGroup.value = Pair(false, response.data)
                }
            }
        }

    }

    suspend fun offlineHistory(credits_semester: Int) {
        databaseImpl.getHistoryDao().updateHistory(
            databaseImpl.getHistoryDao().getHistory()[0].copy(creditsSemester = credits_semester)
        )
    }

    fun updateHistoryDB(credits_semester: Int) {
        viewModelScope.launch {
            offlineHistory(credits_semester)
        }

    }

    fun setAcademicHistory(completed_credits: Int, average: Double) {
        viewModelScope.launch {
            val academic = AcademicItem(average, completed_credits, Singleton.user_id)
            val response = repository.setAcademicHistory(Singleton.token, academic)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val mAcademic = databaseImpl.getHistoryDao().getHistory()
                    _dataResponseSetHistory.value = Pair(true, "Succes")
                }
                Result.Status.ERROR -> {
                    _dataResponseSetHistory.value = Pair(false, response.data)
                }
            }
        }

    }

    fun isNew() {
        viewModelScope.launch {
            if (Singleton.first_login == 1 && Singleton.new_student == 0) {
                _dataResponseNewStudent.value = Pair(true, "Succes")
            } else {
                _dataResponseNewStudent.value = Pair(false, "dialog")
            }
        }
    }

    fun updateStudent(
        career: Int = Singleton.career,
        faculty:Int = Singleton.faculty,
        credits_semester: Int = Singleton.credits_semester,
        first_login: Int = Singleton.first_login,
        new_student: Int = Singleton.new_student
    ) {
        viewModelScope.launch {
            Singleton.career = career
            Singleton.faculty= faculty
            Singleton.credits_semester = credits_semester
            Singleton.first_login = first_login
            Singleton.new_student = new_student

            val updateRequest =
                UpdateRequest(career, faculty,credits_semester, first_login, new_student)
            val response = repository.updateStudent(
                Singleton.token,
                Singleton.user_id,
                updateRequest
            )
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseUpdateStudent.value = Pair(true, "Succes")
                }
                Result.Status.ERROR -> {
                    _dataResponseUpdateStudent.value = Pair(false, response.data)
                }
            }
        }

    }

    fun getCareers() {
        viewModelScope.launch {
            if (!Singleton.isOnline) {
                _dataResponseCareer.value =
                    Pair(true, databaseImpl.getStudentDao().getStudent()[0].careerName)
            } else {
                val response = repository.getSplashCareer(Singleton.token)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        if (response.data != null) {
                            val rawCareer = response.data as List<Data>
                            val listCareer = Gson().toJson(rawCareer)
                            val careerText = Utils().findDataById(Singleton.career, listCareer)
                            val student = databaseImpl.getStudentDao()
                                .getStudent()[0].copy(careerName = careerText)
                            databaseImpl.getStudentDao().updateStudent(student)
                            Singleton.career_name = careerText
                            _dataResponseCareer.value = Pair(true, careerText)
                        } else {
                            _dataResponseCareer.value = Pair(false, response.toString())
                        }
                    }
                    Result.Status.ERROR -> {
                        val dataResult = response.data as? String
                        _dataResponseCareer.value = Pair(false, dataResult)
                    }
                }
            }
        }
    }

    private  fun filter(list: List<Data>): List<Data>{
        val newList = mutableListOf<String>()
        val finalList = mutableListOf<Data>()
        for (i in list) {
            if (!newList.contains(i.name)) {
                newList.add(i.name)
                finalList.add(i)
            }
        }
        return finalList
    }


}