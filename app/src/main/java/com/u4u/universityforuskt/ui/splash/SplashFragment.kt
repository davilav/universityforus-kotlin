package com.u4u.universityforuskt.ui.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.utils.Constants
import org.koin.androidx.viewmodel.ext.android.viewModel

class SplashFragment : Fragment() {

    private val splashViewModel: SplashViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        splashViewModel.isOnline(requireContext())
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        splashViewModel.dataResponseOnline.observe(viewLifecycleOwner, Observer { _ ->
            splashViewModel.getData(requireContext())
        })

        splashViewModel.dataResponse.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    if (response.second as Boolean) {
                        Handler().postDelayed({
                            findNavController().navigate(R.id.action_splashFragment_to_homeFragment)
                        }, Constants.SPLASH_TIME_OUT.toLong())
                    } else {
                        Handler().postDelayed({
                            findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
                        }, Constants.SPLASH_TIME_OUT.toLong())
                    }

                }

                false -> Handler().postDelayed({
                    findNavController().navigate(R.id.action_splashFragment_to_loginFragment)
                }, Constants.SPLASH_TIME_OUT.toLong())
            }
        })

    }

}