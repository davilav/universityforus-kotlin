package com.u4u.universityforuskt.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.u4u.universityforuskt.data.local.db.reminder.Reminder
import kotlinx.android.synthetic.main.layout_reminder.view.*
import java.text.SimpleDateFormat

class ReminderAdapter(
    private var reminderResponses: MutableList<Reminder>
) : RecyclerView.Adapter<ReminderAdapter.ViewHolderData>(), View.OnClickListener {
    private var removedPosition: Int = 0
    private var removedItem: Reminder? = null
    private val parenView: ViewGroup? = null
    var isRemoved: Boolean = true
    private val _userLiveData: MutableLiveData<Any?> = MutableLiveData()
    val userLiveData: LiveData<Any?> get() = _userLiveData

    @NonNull
    override fun onCreateViewHolder(@NonNull parent: ViewGroup, viewType: Int): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(com.u4u.universityforuskt.R.layout.layout_reminder, parenView, false)
        view.setOnClickListener(this)
        return ViewHolderData(view)
    }


    override fun onBindViewHolder(@NonNull holder: ViewHolderData, position: Int) {
        holder.putData(reminderResponses[position])
    }

    override fun getItemCount(): Int {
        return reminderResponses.size
    }

    fun deleteItem(viewholder: RecyclerView.ViewHolder, position: Int) {
        removedPosition = position
        removedItem = reminderResponses[position]

        reminderResponses.removeAt(position)
        notifyItemRemoved(position)
        notifyDataSetChanged()
        isRemoved = true
        Snackbar.make(viewholder.itemView, "$removedItem deleted", Snackbar.LENGTH_LONG)
            .setAction("UNDO") {
                reminderResponses.add(removedPosition, removedItem as Reminder)
                notifyItemInserted(removedPosition)
                notifyDataSetChanged()
                isRemoved = false
            }.show()
    }

    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private var description: TextView = itemView.reminderAdapterDescription
        private var tittle: TextView = itemView.reminderAdapterTittle
        private var time: TextView = itemView.reminderAdapterTime
        fun putData(data: Reminder) {
            description.text = data.description
            tittle.text = data.title
            val df = SimpleDateFormat("MMM dd HH:00")
            val formattedDate = df.format(data.time!!.time)
            time.text = formattedDate

        }

    }

    override fun onClick(p0: View?) {
    }
}