package com.u4u.universityforuskt.ui.register

import android.graphics.Color
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.Register
import com.u4u.universityforuskt.data.models.User
import com.u4u.universityforuskt.data.repository.register.RegisterRepositoryImpl
import kotlinx.coroutines.launch
import java.util.regex.Pattern

@Suppress("UNCHECKED_CAST")
class RegisterViewModel(
    private val repository: RegisterRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponseRegister: MutableLiveData<Pair<Boolean, String?>> = MutableLiveData()
    val dataResponseRegister: LiveData<Pair<Boolean, String?>> get() = _dataResponseRegister

    private val _dataResponseUser: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponseUser: LiveData<Pair<Int, Boolean?>> get() = _dataResponseUser

    private val _dataResponsePassword1: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponsePassword1: LiveData<Pair<Int, Boolean?>> get() = _dataResponsePassword1

    private val _dataResponsePassword2: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponsePassword2: LiveData<Pair<Int, Boolean?>> get() = _dataResponsePassword2

    private val _dataResponseFirstName: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponseFirstName: LiveData<Pair<Int, Boolean?>> get() = _dataResponseFirstName

    private val _dataResponseLastName: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponseLastName: LiveData<Pair<Int, Boolean?>> get() = _dataResponseLastName

    private val _dataResponseEmail: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponseEmail: LiveData<Pair<Int, Boolean?>> get() = _dataResponseEmail

    private val _dataResponseValidateFirst: MutableLiveData<Pair<Int, Boolean?>> = MutableLiveData()
    val dataResponseValidateFirst: LiveData<Pair<Int, Boolean?>> get() = _dataResponseValidateFirst

    private val _dataResponseValidateSecond: MutableLiveData<Pair<Int, Boolean?>> =
        MutableLiveData()
    val dataResponseValidateSecond: LiveData<Pair<Int, Boolean?>> get() = _dataResponseValidateSecond

    private val _dataResponseListUniversities: MutableLiveData<Pair<Boolean, String?>> =
        MutableLiveData()
    val dataResponseListUniversities: LiveData<Pair<Boolean, String?>> get() = _dataResponseListUniversities


    private val _dataResponseListFaculties: MutableLiveData<Pair<Boolean, String?>> =
        MutableLiveData()
    val dataResponseListFaculties: LiveData<Pair<Boolean, String?>> get() = _dataResponseListFaculties

    private val _dataResponseListCareers: MutableLiveData<Pair<Boolean, String?>> =
        MutableLiveData()
    val dataResponseListCareers: LiveData<Pair<Boolean, String?>> get() = _dataResponseListCareers


    fun validateUsername(username: String) {
        viewModelScope.launch {
            if (username.isNotBlank()) {
                if (!Pattern.matches("[a-zA-z_1-9]*", username)) {
                    _dataResponseUser.value = Pair(111, false)
                } else if (!Pattern.matches("[a-zA-z_1-9]{5,}", username)) {
                    _dataResponseUser.value = Pair(112, false)
                } else {
                    _dataResponseUser.value = Pair(200, true)
                }
            } else {
                _dataResponseUser.value = Pair(107, false)
            }
        }
    }

    fun validatePassword1(data: String) {
        viewModelScope.launch {
            if (data.isNotBlank()) {
                if (!Pattern.matches("[a-zA-Z1-9]*", data)) {
                    _dataResponsePassword1.value = Pair(104, false)
                } else if (!Pattern.matches("[a-zA-Z1-9]{8,}", data)) {
                    _dataResponsePassword1.value = Pair(103, false)
                } else {
                    _dataResponsePassword1.value = Pair(200, true)
                }
            } else {
                _dataResponsePassword1.value = Pair(120, false)
            }
        }
    }

    fun validatePassword2(data: String, data2: String) {
        viewModelScope.launch {
            if (data.isNotBlank()) {
                if (data != data2) {
                    _dataResponsePassword2.value = Pair(100, false)
                } else {
                    _dataResponsePassword2.value = Pair(200, true)
                }
            } else {
                _dataResponsePassword2.value = Pair(120, false)
            }
        }
    }

    fun validateFirstName(data: String) {
        viewModelScope.launch {
            if (data.isBlank()) {
                _dataResponseFirstName.value = Pair(116, false)
            } else {
                _dataResponseFirstName.value = Pair(200, true)
            }
        }
    }

    fun validateLastName(data: String) {
        viewModelScope.launch {
            if (data.isBlank()) {
                _dataResponseLastName.value = Pair(117, false)
            } else {
                _dataResponseLastName.value = Pair(200, true)
            }
        }
    }

    fun validateEmail(data: String) {
        viewModelScope.launch {
            if (data.isBlank()) {
                _dataResponseEmail.value = Pair(118, false)

            } else if (!Pattern.matches(
                    "[A-Za-z0-9.]{1,63}@[A-Za-z0-9.]+\\.[a-z]{2,5}.\\.?[a-z]*?",
                    data
                )
            ) {
                _dataResponseEmail.value = Pair(101, false)
            } else {
                _dataResponseEmail.value = Pair(200, true)
            }
        }
    }

    fun validateFirst(data: ArrayList<Any>) {
        viewModelScope.launch {
            if (data[0] != false && data[1] != false && data[2] != false && data[3] != false && data[4] != false) {
                _dataResponseValidateFirst.value = Pair(200, true)
            } else {
                _dataResponseValidateFirst.value = Pair(100, false)
            }
        }
    }

    fun validateSecond(data: ArrayList<Any>) {
        viewModelScope.launch {
            if (data[0] != false && data[1] != false && data[2] != false && data[3] != false && data[4] != false && data[5] != false) {
                _dataResponseValidateSecond.value = Pair(200, true)
            } else {
                _dataResponseValidateSecond.value = Pair(100, false)
            }
        }
    }

    fun paintGenders(
        registerMaleGender: ImageView,
        registerFemaleGender: ImageView,
        registerOtherGender: ImageView
    ) {

        registerMaleGender.setColorFilter(
            Color.argb(
                Color.alpha(Color.WHITE),
                Color.red(Color.WHITE),
                Color.green(Color.WHITE),
                Color.blue(Color.WHITE)
            )
        )
        registerFemaleGender.setColorFilter(
            Color.argb(
                Color.alpha(Color.WHITE),
                Color.red(Color.WHITE),
                Color.green(Color.WHITE),
                Color.blue(Color.WHITE)
            )
        )
        registerOtherGender.setColorFilter(
            Color.argb(
                Color.alpha(Color.WHITE),
                Color.red(Color.WHITE),
                Color.green(Color.WHITE),
                Color.blue(Color.WHITE)
            )
        )
    }

    fun getUniversityData() {
        viewModelScope.launch {
            val response = repository.getUniversityData()
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val listUniversities = response.data as? String
                    _dataResponseListUniversities.value = Pair(true, listUniversities)
                }
                Result.Status.ERROR -> {
                    val dataResult = response.data as? String
                    _dataResponseListUniversities.value = Pair(false, dataResult)
                }
            }

        }
    }

    fun setRegisterUser(
        birth_date: String,
        career: Int,
        credits_semester: Int,
        faculty: Int,
        first_login: Int,
        gender: String,
        new_student: Int,
        university: Int,
        email: String,
        first_name: String,
        last_name: String,
        password1: String,
        password2: String,
        username: String
    ) {
        val user = User(email, first_name, last_name, password1, password2, username)
        val register = Register(
            birth_date,
            career,
            credits_semester,
            faculty,
            first_login,
            gender,
            new_student,
            university,
            user
        )
        viewModelScope.launch {
            val response = repository.setRegisterUser(register)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseRegister.value = Pair(true, "")
                }
                Result.Status.ERROR -> {
                    val dataResult = response.data as? String
                    _dataResponseRegister.value = Pair(false, dataResult)
                }
            }
        }

    }

    fun getFacultyData(id: Int) {
        viewModelScope.launch {
            val response = repository.getFacultyData(id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val rawFaculties = response.data as List<Data>
                    val listFaculties = Gson().toJson(rawFaculties)
                    _dataResponseListFaculties.value = Pair(true, listFaculties)
                }
                Result.Status.ERROR -> {
                    val dataResult = response.data as? String
                    _dataResponseListFaculties.value = Pair(false, dataResult)
                }

            }

        }
    }

    fun getCareerData(id: Int) {
        viewModelScope.launch {
            val response = repository.getCareerData(id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val rawCareer = response.data as List<Data>
                    val listCareer = Gson().toJson(rawCareer)
                    _dataResponseListCareers.value = Pair(true, listCareer)
                }
                Result.Status.ERROR -> {
                    val dataResult = response.data as? String
                    _dataResponseListCareers.value = Pair(false, dataResult)
                }
            }
        }
    }

    fun showHide(view: View) {
        view.visibility = if (view.visibility == View.VISIBLE) {
            View.INVISIBLE
        } else {
            View.VISIBLE
        }
    }
}