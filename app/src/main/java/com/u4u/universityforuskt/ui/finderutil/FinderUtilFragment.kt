package com.u4u.universityforuskt.ui.finderutil

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.ui.dialogs.SelectOptionDialog
import kotlinx.android.synthetic.main.fragment_finder_util.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("UNCHECKED_CAST")
class FinderUtilFragment : Fragment() {

    private val viewModel: FinderUtilViewModel by viewModel()
    var subjectId: Int = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_finder_util, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setOnClickListener()

        viewModel.dataResponseSubjects.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    SelectOptionDialog.newInstance(response.second as String,
                        object : SelectOptionDialog.OnListenerInterface {
                            override fun optionSelect(data: Data?) {
                                if (data != null) {
                                    subjectId = data.subject_id
                                    textViewSurveyDialogSubject.text = data.name
                                }
                            }
                        }, "materias"
                    ).show(childFragmentManager, "")
                }
                false -> {
                    Toast.makeText(requireContext(), response.second as String, Toast.LENGTH_SHORT)
                        .show()
                }
            }

        })
        viewModel.dataResponseNewComment.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    findNavController().navigate(R.id.action_finderUtilFragment_to_finderFragment)
                }
                false -> {
                    Toast.makeText(requireContext(), response.second as String, Toast.LENGTH_SHORT)
                        .show()
                }
            }

        })
    }

    private fun setOnClickListener() {
        textViewSurveyDialogSubject.setOnClickListener {
            val teacherId = arguments?.getInt("teacher_id")
            if (teacherId != null && teacherId != 0) {
                viewModel.getTeacherSubjects(teacherId.toInt())
            }
        }
        teacherSummitButton.setOnClickListener {
            val teacherId = arguments?.getInt("teacher_id")
            if (teacherId != null && teacherId != 0 && subjectId != 0) {
                viewModel.addNewComment(
                    ratingBarSurvey2.rating.toDouble(),
                    ratingBarSurvey5.rating.toDouble(),
                    ImageViewSurveyDescriptionField.text.toString(),
                    ratingBarSurvey4.rating.toDouble(),
                    ratingBarSurvey1.rating.toDouble(),
                    editTextSurveyFinalNoteField.text.toString().toDouble(),
                    editTextSurveySemesterField.text.toString(),
                    ratingBarSurvey3.rating.toDouble(),
                    subjectId,
                    teacherId
                )
            }
        }
    }


}