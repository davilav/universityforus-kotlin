package com.u4u.universityforuskt.ui.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data


class FinderAdapter(private val teachers: List<Data>) :
    RecyclerView.Adapter<FinderAdapter.ViewHolderData>(), View.OnClickListener {
    private var listener: View.OnClickListener? = null
    private val parentView: ViewGroup? = null

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_finder, parentView, false)
        view.setOnClickListener(this)
        return ViewHolderData(view)
    }

    override fun onBindViewHolder(
        @NonNull holder: ViewHolderData,
        position: Int
    ) {
        holder.putData(teachers[position])
    }

    override fun getItemCount(): Int {
        return teachers.size
    }

    fun setOnClickListener(listener: View.OnClickListener?) {
        this.listener = listener
    }

    override fun onClick(view: View?) {
        listener?.onClick(view)
    }

    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private var teachers: TextView = itemView.findViewById(R.id.defaultTextViewTeachersAdapter)
        fun putData(data: Data) {
            teachers.text = data.name
        }

    }


}
