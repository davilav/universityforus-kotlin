package com.u4u.universityforuskt.ui.reminder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import kotlinx.coroutines.launch

class ReminderViewModel(private val databaseImpl: LocalDataBase) : ViewModel() {
    private val _dataResponseReminder: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseReminder: LiveData<Pair<Boolean, Any?>> get() = _dataResponseReminder

    fun getReminders(subjectId: Long) {
        viewModelScope.launch {

        }
    }
}
