package com.u4u.universityforuskt.ui.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import java.util.*
import kotlin.collections.ArrayList

@Suppress("UNCHECKED_CAST")
class SelectOptionAdapter(
    val arrayData: ArrayList<Data>?,
    val adapterListener: OnAdapterListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {
    private var arrayDataFiltered: ArrayList<Data>? = arrayData


    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_item_option, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(
        @NonNull holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        (holder as ItemViewHolder).bindData(this.arrayDataFiltered!![position])
    }

    override fun getItemCount(): Int {
        return arrayDataFiltered!!.size
    }

    override fun getFilter(): Filter? {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults? {
                val charString = charSequence.toString()
                arrayDataFiltered = if (charString.isEmpty()) {
                    arrayData
                } else {
                    val filteredList: ArrayList<Data> = ArrayList()
                    for (data in arrayData!!) {
                        if (data.name.toLowerCase(Locale.ROOT)
                                .contains(charString.toLowerCase(Locale.ROOT)) || data.id.toString()
                                .contains(charSequence)
                        ) {
                            filteredList.add(data)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = arrayDataFiltered
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence?,
                filterResults: FilterResults
            ) {
                arrayDataFiltered = filterResults.values as ArrayList<Data>
                notifyDataSetChanged()
            }
        }
    }

    inner class ItemViewHolder(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val titleItem: TextView = itemView.findViewById(R.id.titleOption)

        fun bindData(data: Data) {
            titleItem.text = data.name
            itemView.setOnClickListener { adapterListener.itemSelect(data) }
        }

    }

    interface OnAdapterListener {
        fun itemSelect(data: Data)
    }
}


