package com.u4u.universityforuskt.ui.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.local.db.history.History
import com.u4u.universityforuskt.data.local.db.student.Student
import com.u4u.universityforuskt.data.models.Academic
import com.u4u.universityforuskt.data.models.Summary
import com.u4u.universityforuskt.data.models.UpdateRequest
import com.u4u.universityforuskt.data.repository.profile.ProfileRepositoryImpl
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.launch

class ProfileViewModel(
    private val repository: ProfileRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponseAcademicHistory: MutableLiveData<Pair<Boolean, Any?>> =
        MutableLiveData()
    val dataResponseAcademicHistory: LiveData<Pair<Boolean, Any?>> get() = _dataResponseAcademicHistory

    private val _dataResponseLogout: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseLogout: LiveData<Pair<Boolean, Any?>> get() = _dataResponseLogout

    private val _dataResponseCareer: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseCareer: LiveData<Pair<Boolean, Any?>> get() = _dataResponseCareer

    private val _dataResponseFaculty: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseFaculty: LiveData<Pair<Boolean, Any?>> get() = _dataResponseFaculty

    private val _dataResponseDeleteHistory: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseDeleteHistory: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteHistory

    private val _dataResponseDeleteSubjects: MutableLiveData<Pair<Boolean, Any?>> =
        MutableLiveData()
    val dataResponseDeleteSubjects: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteSubjects

    private val _dataResponseDeleteNote: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseDeleteNote: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteNote

    private val _dataResponseUpdateStudent: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseUpdateStudent: LiveData<Pair<Boolean, Any?>> get() = _dataResponseUpdateStudent

    private val _dataResponseSubjectsTaken: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSubjectsTaken: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSubjectsTaken

    private suspend fun logoutDB() {
        val studentDB = Student(
            Singleton.user_id,
            Singleton.token,
            Singleton.first_name,
            Singleton.last_name,
            Singleton.username,
            Singleton.career,
            Singleton.faculty,
            Singleton.email,
            Singleton.university,
            0,
            Singleton.career_name
        )
        databaseImpl.getStudentDao().updateStudent(studentDB)
    }

    fun getAcademicHistory() {
        viewModelScope.launch {
            val isOfflineHistoryEmpty = isOfflineHistoryEmpty()
            if (isOfflineHistoryEmpty != null && !Singleton.isOnline) {
                _dataResponseAcademicHistory.value = Pair(true, isOfflineHistoryEmpty)
            } else {
                val response = repository.getAcademicHistory(Singleton.token, Singleton.user_id)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        if (response.data != null) {
                            val data = response.data as List<Academic>
                            if (data.isEmpty()) {
                                _dataResponseAcademicHistory.value =
                                    Pair(false, "no se encontro informacion")
                            } else {
                                offlineHistory(data[0])
                                _dataResponseAcademicHistory.value = Pair(true, data)
                            }
                        } else {
                            Pair(false, "no se encontro informacion")
                        }
                    }
                    Result.Status.ERROR -> {
                        _dataResponseAcademicHistory.value =
                            Pair(false, "no se encontro informacion")
                    }
                }
            }
        }
    }

    private suspend fun offlineHistory(data: Academic) {
        if (databaseImpl.getHistoryDao().getHistory().isNullOrEmpty()) {
            databaseImpl.getHistoryDao().insertHistory(
                History(
                    data.id,
                    data.credits_number,
                    data.credits_semester,
                    data.completed_credits,
                    data.average,
                    data.progress
                )
            )
        } else {
            databaseImpl.getHistoryDao().updateHistory(
                History(
                    data.id,
                    data.credits_number,
                    data.credits_semester,
                    data.completed_credits,
                    data.average,
                    data.progress
                )
            )
        }
    }

    private suspend fun isOfflineHistoryEmpty(): List<Academic>? {
        if (!databaseImpl.getHistoryDao().getHistory().isNullOrEmpty()) {
            val historyDB = databaseImpl.getHistoryDao().getHistory()
            return listOf(
                Academic(
                    historyDB[0].average!!,
                    historyDB[0].completedCredits!!,
                    historyDB[0].creditsNumber!!,
                    historyDB[0].creditsSemester!!,
                    historyDB[0].id,
                    historyDB[0].progress!!,
                    ""
                )
            )
        }
        return null
    }


    fun deleteHistory() {
        viewModelScope.launch {
            val response = repository.deleteHistory(Singleton.token, Singleton.user_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseDeleteHistory.value = Pair(true, "success")
                }
                Result.Status.ERROR -> {
                    _dataResponseDeleteHistory.value = Pair(false, "no se encontro informacion")
                }
            }
        }

    }

    fun deleteNotes() {
        viewModelScope.launch {
            val response = repository.deleteAllNotes(Singleton.token, Singleton.user_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseDeleteNote.value = Pair(true, "success")
                }
                Result.Status.ERROR -> {
                    _dataResponseDeleteNote.value = Pair(false, "no se encontro informacion")
                }
            }
        }

    }

    fun deleteSubjects() {
        viewModelScope.launch {
            val response = repository.deleteAllSubjects(Singleton.token, Singleton.user_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseDeleteSubjects.value = Pair(true, "success")
                }
                Result.Status.ERROR -> {
                    _dataResponseDeleteSubjects.value = Pair(false, "no se encontro informacion")
                }
            }
        }

    }

    fun updateStudent(
        career: Int = Singleton.career,
        faculty: Int = Singleton.faculty,
        credits_semester: Int = Singleton.credits_semester,
        first_login: Int = Singleton.first_login,
        new_student: Int = Singleton.new_student
    ) {
        viewModelScope.launch {
            Singleton.career = career
            Singleton.faculty = faculty
            Singleton.credits_semester = credits_semester
            Singleton.first_login = first_login
            Singleton.new_student = new_student

            val updateRequest =
                UpdateRequest(career, faculty, credits_semester, first_login, new_student)
            val response = repository.updateStudent(
                "Token " + Singleton.token,
                Singleton.user_id,
                updateRequest
            )
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseUpdateStudent.value = Pair(true, "Success")
                }
                Result.Status.ERROR -> {
                    _dataResponseUpdateStudent.value = Pair(false, response.data)
                }
            }
        }

    }

    fun logout() {
        viewModelScope.launch {
            if (Singleton.isOnline) {
                val response = repository.logout(Singleton.token)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        _dataResponseLogout.value = Pair(true, "success")
                    }
                    Result.Status.ERROR -> {
                        _dataResponseLogout.value = Pair(false, "no se encontro informacion")
                    }
                }
            } else {
                logoutDB()
                _dataResponseLogout.value = Pair(true, "success")
            }

        }

    }

    fun getCareer(faculty_id: Int) {
        viewModelScope.launch {
            val response = repository.getCareer(faculty_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseCareer.value = Pair(true, response.data)
                }
                Result.Status.ERROR -> {
                    _dataResponseCareer.value = Pair(false, "no se encontro informacion")
                }
            }
        }

    }

    fun getFaculty() {
        viewModelScope.launch {
            val response = repository.getFaculty(Singleton.university)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseFaculty.value = Pair(true, response.data)
                }
                Result.Status.ERROR -> {
                    _dataResponseFaculty.value = Pair(false, "no se encontro informacion")
                }
            }
        }

    }

    fun subjectsTaken() {
        viewModelScope.launch {
            val isOfflineSubjectsTakenEmpty = isOfflineSubjectsTakenEmpty()
            if (isOfflineSubjectsTakenEmpty != null && !Singleton.isOnline) {
                _dataResponseSubjectsTaken.value = Pair(true, isOfflineSubjectsTakenEmpty)
            } else {
                val response = repository.subjectsTaken(Singleton.token, Singleton.user_id)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        offlineSubjectsTaken(response.data as List<Summary>)
                        _dataResponseSubjectsTaken.value = Pair(true, response.data)
                    }
                    Result.Status.ERROR -> {
                        _dataResponseSubjectsTaken.value = Pair(false, "no se encontro informacion")
                    }
                }
            }
        }
    }

    private suspend fun offlineSubjectsTaken(summary: List<Summary>) {
        if (databaseImpl.getSummaryDao().getSummary().isNullOrEmpty()) {
            for (data in summary) {
                databaseImpl.getSummaryDao().insertSummary(
                    com.u4u.universityforuskt.data.local.db.summary.Summary(
                        data.id,
                        data.semester,
                        data.student,
                        data.subject,
                        data.value,
                        data.year
                    )
                )
            }
        } else {
            for (data in summary) {
                databaseImpl.getSummaryDao().updateSummary(
                    com.u4u.universityforuskt.data.local.db.summary.Summary(
                        data.id,
                        data.semester,
                        data.student,
                        data.subject,
                        data.value,
                        data.year
                    )
                )
            }
        }

    }

    private suspend fun isOfflineSubjectsTakenEmpty(): List<Summary>? {
        if (!databaseImpl.getSummaryDao().getSummary().isNullOrEmpty()) {
            val subjectsTakenDB = databaseImpl.getSummaryDao().getSummary()
            val newList = mutableListOf<Summary>()
            for (summary in subjectsTakenDB) {
                newList.add(
                    Summary(
                        summary.id,
                        summary.semester!!,
                        summary.student!!,
                        summary.subject!!,
                        summary.value!!,
                        summary.year!!
                    )
                )
            }
            return newList
        }
        return null
    }

}

