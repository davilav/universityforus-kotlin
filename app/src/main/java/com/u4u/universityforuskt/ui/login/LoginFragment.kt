package com.u4u.universityforuskt.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.android.synthetic.main.login_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.login_fragment, container, false)
    }

    private fun setOnclickListener() {
        loginButton.setOnClickListener {
            if (Singleton.isOnline) {
                viewModel.validateData(
                    loginLayoutUserTxt.text.toString(),
                    loginLayoutPasswordTxt.text.toString()
                )
            } else {
                Snackbar.make(requireView(), "No es posible sin conexion", Snackbar.LENGTH_SHORT)
                    .show()
            }

        }
        if (!Singleton.isOnline) {
            loginToRegisterButton.visibility = View.GONE
        }
        loginToRegisterButton.setOnClickListener { findNavController().navigate(R.id.action_loginFragment_to_registerFragment) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setOnclickListener()
        viewModel.dataResponse.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
                false -> Toast.makeText(context, response.second, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.dataResponsePass.observe(viewLifecycleOwner, Observer { response ->
            if (!response.first) {
                loginLayoutPasswordTxt.requestFocus()
                loginLayoutPasswordTxt.error = response.second
            }

        })
        viewModel.dataResponseUser.observe(viewLifecycleOwner, Observer { response ->
            if (!response.first) {
                loginLayoutUserTxt.requestFocus()
                loginLayoutUserTxt.error = response.second
            }

        })
    }
}