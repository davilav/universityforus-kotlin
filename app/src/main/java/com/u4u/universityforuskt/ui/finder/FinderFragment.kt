package com.u4u.universityforuskt.ui.finder

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Comment
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.TeachersAverageResponseItem
import com.u4u.universityforuskt.ui.adapters.FinderAdapter
import com.u4u.universityforuskt.ui.adapters.FinderCommentAdapter
import com.u4u.universityforuskt.utils.Utils
import kotlinx.android.synthetic.main.finder_fragment.*
import kotlinx.android.synthetic.main.layout_comments.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("UNCHECKED_CAST")
class FinderFragment : Fragment() {

    private val viewModel: FinderViewModel by viewModel()
    private var teacherId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.finder_fragment, container, false)
    }

    private val commentScrollListener: RecyclerView.OnScrollListener =
        object : RecyclerView.OnScrollListener() {

            override fun onScrolled(
                @NonNull recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                viewModel.isLastItemDisplaying(recyclerView)
            }
        }

    private fun setOnClickListener() {
        addReferenceFinderButton.setOnClickListener {
            val bundle = bundleOf("teacher_id" to teacherId)
            findNavController().navigate(R.id.action_finderFragment_to_finderUtilFragment, bundle)
        }
        textViewCommentsViewMoreFinder.setOnClickListener {
            viewModel.getAllTeacherComments(teacherId)
        }
        searchViewFinderTxt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (recyclerFinder.visibility == View.INVISIBLE || recyclerFinder.visibility == View.GONE) {
                    commentsLayoutFinder.visibility = View.INVISIBLE
                    teacherLayoutFinder.visibility = View.INVISIBLE
                    recyclerFinder.visibility = View.VISIBLE
                }
                viewModel.getFindTeachers(p0.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setOnClickListener()
        val view = View.inflate(requireContext(), R.layout.layout_comments, null)
        var adapter: FinderCommentAdapter?
        view.recyclerCommentFinder.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        val commentsDialog = BottomSheetDialog(requireContext())
        commentsDialog.setContentView(view)
        view.recyclerCommentFinder.addOnScrollListener(commentScrollListener)
        Utils().showKeyboard(searchViewFinderTxt, requireContext())
        if (recyclerFinder.visibility == View.VISIBLE) {
            searchViewFinderTxt.requestFocus()
        }

        viewModel.dataResponseFinder.observe(viewLifecycleOwner,androidx.lifecycle.Observer { response ->
            when (response.first) {
                true -> {
                    recyclerFinder.layoutManager = LinearLayoutManager(
                        requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                    val finderAdapter = FinderAdapter(response.second as List<Data>)
                    finderAdapter.setOnClickListener(View.OnClickListener { view ->
                        val string =
                            (response.second as List<Data>)[recyclerFinder.getChildAdapterPosition(
                                view
                            )].name.split(" ")
                        val firstName = StringBuilder()
                        val lastName = StringBuilder()

                        for (i in string.indices) {
                            if (i <= 1) {
                                firstName.append(string[i]).append(" ")
                            } else {
                                lastName.append(string[i]).append(" ")
                            }
                        }
                        teacherId =
                            (response.second as ArrayList<Data>)[recyclerFinder.getChildAdapterPosition(
                                view
                            )].id
                        viewModel.getTeacherComments(teacherId)
                    })
                    recyclerFinder.adapter = finderAdapter

                }
                false -> {
                }
            }

        })
        viewModel.dataResponseComments.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    textViewSubjectFirstCommentFinder.visibility = View.VISIBLE
                    textViewCommentFirstNoteFinder.visibility = View.VISIBLE
                    textViewSubjectFirstCommentFinder.visibility = View.VISIBLE
                    textViewCommentsViewMoreFinder.visibility = View.VISIBLE
                    textViewSubjectFirstCommentFinder.text =
                        (response.second as List<Comment>)[0].name
                    textViewCommentFirstNoteFinder.text =
                        "Nota final = " + (response.second as List<Comment>)[0].final_note.toString()
                    textViewFirstCommentFinder.text =
                        (response.second as List<Comment>)[0].description
                    recyclerFinder.visibility = View.GONE
                    viewModel.getTeacherAverages(teacherId)

                }
                false -> {
                    viewModel.getTeacherAverages(teacherId)
                    textViewSubjectFirstCommentFinder.visibility = View.GONE
                    textViewCommentFirstNoteFinder.visibility = View.GONE
                    textViewSubjectFirstCommentFinder.visibility = View.GONE
                    textViewCommentsViewMoreFinder.visibility = View.GONE
                }
            }

        })
        viewModel.dataResponseAllComments.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    if (viewModel.page == 1) {
                        adapter = FinderCommentAdapter((response.second as List<Comment>))
                        view.recyclerCommentFinder.adapter = adapter
                        view.recyclerCommentFinder.scrollToPosition(0)
                        commentsDialog.show()
                        commentsDialog.setOnDismissListener { viewModel.resetComments() }
                        commentsDialog.setOnCancelListener { viewModel.resetComments() }
                    } else {
                        adapter = FinderCommentAdapter((response.second as List<Comment>))
                        view.recyclerCommentFinder.adapter = adapter
                        view.recyclerCommentFinder.scrollToPosition(((response.second as List<Comment>).size) - 1)
                    }

                }
                false -> {
                }
            }

        })
        viewModel.dataResponseItemDisplay.observe(viewLifecycleOwner, androidx.lifecycle.Observer { response ->
            when (response.first) {
                true -> {
                    viewModel.getAllTeacherComments(teacherId)
                }
                false -> {
                }
            }

        })
        viewModel.dataResponseAverages.observe(viewLifecycleOwner,androidx.lifecycle.Observer { response ->
            when (response.first) {
                true -> {
                    Utils().closeKeyboard(requireView(), requireContext())
                    commentsLayoutFinder.visibility = View.VISIBLE
                    teacherLayoutFinder.visibility = View.VISIBLE
                    recyclerFinder.visibility = View.GONE
                    searchViewFinderTxt.clearFocus()
                    textViewNameFinder.text =
                        (response.second as TeachersAverageResponseItem).name
                    textViewNoteFinder.text =
                        (response.second as TeachersAverageResponseItem).note_average.toString()
                    noteField1Finder.text =
                        (response.second as TeachersAverageResponseItem).explication_average.toString()
                    noteField2Finder.text =
                        (response.second as TeachersAverageResponseItem).amenity_average.toString()
                    noteField3Finder.text =
                        (response.second as TeachersAverageResponseItem).classes_average.toString()
                    noteField4Finder.text =
                        (response.second as TeachersAverageResponseItem).experience_average.toString()
                    noteField5Finder.text =
                        (response.second as TeachersAverageResponseItem).student_average.toString()
                }
                false -> {
                    Utils().closeKeyboard(requireView(), requireContext())
                }
            }

        })

    }

}