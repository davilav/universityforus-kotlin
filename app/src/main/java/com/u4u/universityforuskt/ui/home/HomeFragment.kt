package com.u4u.universityforuskt.ui.home

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.alamkanak.weekview.WeekView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.Event
import com.u4u.universityforuskt.ui.adapters.ScheduleAdapter
import com.u4u.universityforuskt.ui.dialogs.SelectOptionDialog
import com.u4u.universityforuskt.utils.Singleton
import com.u4u.universityforuskt.utils.Utils
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.android.synthetic.main.layout_add_subject.view.*
import kotlinx.android.synthetic.main.layout_profile_info.view.*
import kotlinx.android.synthetic.main.subject_info.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*


@Suppress("UNCHECKED_CAST")
class HomeFragment : Fragment() {
    private val notificationsTime: TextView? = null
    private val settings: SharedPreferences? = null
    private val viewModel: HomeViewModel by viewModel()
    private val nullParent: ViewGroup? = null
    private val dateFormatter = SimpleDateFormat("EEE", Locale.getDefault())
    private val weekView: WeekView<Event> by lazy {
        requireActivity().findViewById<WeekView<Event>>(R.id.weekView)
    }
    var credits = Singleton.credits_semester

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    private fun setOnClickListener() {

        homeFloatingAddButton.setOnClickListener {
            viewModel.getSubjects(Singleton.career)
        }
        weekView.setOnEventClickListener { data, _ ->
            val view = View.inflate(requireContext(), R.layout.subject_info, null)
            val subjectInfoDialog = BottomSheetDialog(requireContext())
            subjectInfoDialog.setContentView(view)
            view.subjectNameTxt.text = data.subject_name
            view.teacherNameTxt.text = """${data.credits} creditos"""
            view.toNotesText.setOnClickListener {
                val id = data.id.toInt()
                val bundle = bundleOf("subject_id" to id)
                subjectInfoDialog.dismiss()
                findNavController().navigate(R.id.action_homeFragment_to_subjectFragment2, bundle)
            }
            view.deleteText.setOnClickListener {
                viewModel.deleteGroup(data.class_id.toInt())
                val cre = credits - data.credits
                if (cre < 0) {
                    credits = 0
                } else {
                    credits -= data.credits
                }
                subjectInfoDialog.dismiss()
            }
            view.toReminderText.setOnClickListener {
                val id = data.id.toInt()
                val bundle = bundleOf("subject_id" to id)
                subjectInfoDialog.dismiss()
                findNavController().navigate(R.id.action_homeFragment_to_reminderFragment, bundle)
            }
            subjectInfoDialog.show()
        }
        weekView.setOnEmptyViewLongClickListener {
            Toast.makeText(context, "hello", Toast.LENGTH_LONG).show()
        }
        imageButtonEditProfileHome.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_profileFragment)
        }
        searchViewHome.setOnClickListener { findNavController().navigate(R.id.action_homeFragment_to_finderFragment) }
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (!Singleton.isOnline) {
            searchViewHome.visibility = android.view.View.GONE
            homeFloatingAddButton.visibility = android.view.View.GONE
        }
        viewModel.wakeUp()
        viewModel.getEvents(requireView())
        viewModel.isNew()
        viewModel.getCareers()
        var view: View = View.inflate(requireContext(), R.layout.layout_add_subject, null)
        setOnClickListener()
        textViewHomeName.text = Singleton.first_name + " " + Singleton.last_name
        var groupId = 0
        weekView.setDateFormatter { calendar -> dateFormatter.format(calendar.time) }
        viewModel.dataResponseEvents.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        var timeIndex = -1
                        val actualTime = Calendar.getInstance()
                        val nextBlock = Calendar.getInstance()
                        if ((response.second as List<Event>).isNotEmpty()) {
                            for ((i, event) in (response.second as List<Event>).withIndex()) {
                                if (event.startTime.after(actualTime)) {
                                    val mTime = event.startTime.timeInMillis - 900000
                                    buildAlarm(i, mTime)
                                }

                            }
                            nextBlock.set(
                                Calendar.HOUR_OF_DAY,
                                nextBlock.get(Calendar.HOUR_OF_DAY) + 2
                            )
                            for ((i, event) in (response.second as List<Event>).withIndex()) {
                                if (event.startTime.after(actualTime) && event.startTime.before(
                                        nextBlock
                                    )
                                ) {
                                    timeIndex = i
                                }
                            }
                            if (timeIndex < 0) {
                                site.text = ""
                                next_class_Name_TextView.text =
                                    "No hay clases programadas para las siguiente dos Horas"
                                schedule.text = ""
                            } else {
                                if ((response.second as List<Event>)[timeIndex].building_name != null && (response.second as List<Event>)[timeIndex].classroom_Name != null) {
                                    site.text =
                                        (response.second as List<Event>)[timeIndex].building_name.toString() + ": " + (response.second as List<Event>)[timeIndex].classroom_Name.toString()
                                    next_class_Name_TextView.text =
                                        (response.second as List<Event>)[timeIndex].subject_name.toString()
                                    schedule.text =
                                        """${(response.second as List<Event>)[timeIndex].startTime.get(
                                            Calendar.HOUR
                                        )}:00 - ${(response.second as List<Event>)[timeIndex].endTime.get(
                                            Calendar.HOUR
                                        )}:00"""
                                }
                            }

                            weekView.submit(response.second as List<Event>)
                            weekView.notifyDataSetChanged()
                            weekView.refreshDrawableState()
                        }
                    }
                    false -> {
                    }
                }

            })

        viewModel.dataResponseSubjects.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        view = View.inflate(requireContext(), R.layout.layout_add_subject, null)
                        val arraySubject = Gson().toJson(response.second as List<Data>)
                        val subjectDialog = BottomSheetDialog(requireContext())
                        subjectDialog.setContentView(view)
                        credits = Singleton.credits_semester
                        view.textViewLayoutSubjectCreditsTxt.text = credits.toString()
                        view.textViewLayoutSubjectNameTxt.setOnClickListener {
                            SelectOptionDialog.newInstance(
                                arraySubject, object : SelectOptionDialog.OnListenerInterface {
                                    override fun optionSelect(data: Data?) {
                                        if (data != null) {
                                            val params: ViewGroup.LayoutParams =
                                                view.textViewLayoutSubjectRecycler.layoutParams
                                            params.height = 1
                                            view.textViewLayoutSubjectRecycler.layoutParams = params
                                            view.textViewLayoutSubjectNameTxt.text = data.name
                                            viewModel.getGroups(data.id, data.name)
                                            view.textViewLayoutSubjectGroupTxt.isEnabled = true
                                            view.textViewLayoutSubjectGroupTxt.text =
                                                resources.getText(R.string.group)
                                            view.textViewLayoutSubjectTeacherTxt.text =
                                                resources.getText(R.string.teacher)
                                        }
                                    }
                                }
                                , "materias").show(childFragmentManager, "")
                        }
                        subjectDialog.show()
                        view.dialogCancelButton.setOnClickListener { subjectDialog.dismiss() }
                        view.dialogOkButton.setOnClickListener {
                            if (view.textViewLayoutSubjectTeacherTxt.text.toString()
                                    .toLowerCase(Locale.ROOT) != resources.getText(R.string.teacher)
                            ) {
                                viewModel.setNotes(groupId)
                                viewModel.setGroups(groupId)
                                subjectDialog.dismiss()

                            }
                        }
                    }
                    false -> {
                        view.dialogOkButton.isEnabled = false
                        view.dialogOkButton.setTextColor(resources.getColor(R.color.icons))
                        Toast.makeText(
                            requireContext(),
                            response.second as String,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseGroups.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        val arrayGroup = Gson().toJson(response.second)
                        view.textViewLayoutSubjectGroupTxt.setOnClickListener {
                            SelectOptionDialog.newInstance(arrayGroup,
                                object : SelectOptionDialog.OnListenerInterface {
                                    override fun optionSelect(data: Data?) {
                                        if (data != null) {
                                            groupId = data.id
                                            credits =
                                                Singleton.credits_semester + data.credits_number
                                            view.textViewLayoutSubjectCreditsTxt.text =
                                                credits.toString()
                                            view.textViewLayoutSubjectGroupTxt.text = data.name
                                            view.textViewLayoutSubjectTeacherTxt.text =
                                                data.teacher_name
                                            viewModel.getSchedule(data.subject_id, data.id)
                                        }
                                    }
                                }, "Grupos"
                            ).show(childFragmentManager, "")
                        }
                    }
                    false -> {
                        view.dialogOkButton.isEnabled = false
                        view.dialogOkButton.setTextColor(resources.getColor(R.color.icons))
                        Toast.makeText(
                            requireContext(),
                            response.second as String,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseSchedule.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        val params: ViewGroup.LayoutParams =
                            view.textViewLayoutSubjectRecycler.layoutParams
                        params.height = 550
                        view.textViewLayoutSubjectRecycler.layoutParams = params
                        view.textViewLayoutSubjectRecycler.layoutManager =
                            GridLayoutManager(requireContext(), 2)
                        view.dialogOkButton.isEnabled = true
                        view.dialogOkButton.setTextColor(resources.getColor(R.color.u4u_blue))
                        val scheduleAdapter = ScheduleAdapter(response.second as List<Data>)
                        view.textViewLayoutSubjectRecycler.adapter = scheduleAdapter
                    }
                    false -> {
                        view.dialogOkButton.isEnabled = false
                        view.dialogOkButton.setTextColor(resources.getColor(R.color.icons))
                        Toast.makeText(
                            requireContext(),
                            response.second as String,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseSetGroups.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        viewModel.getEvents(requireView())
                        viewModel.updateStudent(
                            Singleton.career,
                            Singleton.faculty,
                            credits,
                            Singleton.first_login,
                            Singleton.new_student
                        )
                    }
                    false -> {
                        Toast.makeText(
                            requireContext(),
                            "no se pudo añadir el grupo",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseUpdateStudent.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        viewModel.updateHistoryDB(credits)
                    }
                    false -> {
                        Toast.makeText(
                            requireContext(),
                            "no se pudo actualizar el numero de creditos",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseSetNotes.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                    }
                    false -> {
                        Toast.makeText(
                            requireContext(),
                            "no se pudo añadir",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseDeleteGroup.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        viewModel.getEvents(requireView())
                        viewModel.updateStudent(
                            career = Singleton.career,
                            credits_semester = credits,
                            first_login = Singleton.first_login,
                            new_student = Singleton.new_student
                        )
                    }
                    false -> {
                        Toast.makeText(
                            requireContext(),
                            "no se pudo eliminar la materia",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseNewStudent.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        val viewNew =
                            layoutInflater.inflate(R.layout.layout_profile_info, nullParent)
                        val academicHistoryDialog = Dialog(requireContext())
                        academicHistoryDialog.setContentView(viewNew)
                        viewNew.dialogOkButtonAcademic.setOnClickListener {
                            val credits =
                                viewNew.academicHistoryCreditsLayoutTxt.text.toString().toInt()
                            val average =
                                viewNew.academicHistoryAverageLayoutTxt.text.toString().toDouble()
                            viewModel.setAcademicHistory(credits, average)
                            academicHistoryDialog.dismiss()
                        }
                        academicHistoryDialog.show()
                    }
                    false -> {
                    }
                }
            })

        viewModel.dataResponseSetHistory.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        viewModel.updateStudent(Singleton.career, Singleton.credits_semester, 1, 1)
                    }
                    false -> {
                        Toast.makeText(
                            requireContext(),
                            "no se pudo añadir la historia academica",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            })

        viewModel.dataResponseCareer.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { response ->
                when (response.first) {
                    true -> {
                        Singleton.career_name = response.second as String
                        textViewHomeCareer.text = response.second
                    }
                    false -> {
                    }
                }
            })
    }

    fun buildAlarm(alarmID: Int, time: Long) {
        val util = Utils()
        util.setAlarm(alarmID, time, requireContext())
    }

}