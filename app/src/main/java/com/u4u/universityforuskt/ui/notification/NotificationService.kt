package com.u4u.universityforuskt.ui.notification


import android.app.*
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.ui.BaseActivity

var name: String? = "SERVICE"

class NotificationService() : IntentService(name) {

    private var notificationManager: NotificationManager? = null
    private var pendingIntent: PendingIntent? = null
    private val NOTIFICATION_ID = 1
    var notification: Notification? = null


    constructor(mName: String) : this() {
        name = mName
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onHandleIntent(p0: Intent?) {
        val NOTIFICATION_CHANNEL_ID = "id.mientras"
        val context: Context = this.applicationContext
        notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        val mIntent = Intent(this, BaseActivity::class.java)
        val res: Resources = this.resources
        val soundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)

        val message = "Te quedan 15 minutos para tu siguiente clase!"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val NOTIFY_ID = 0 // ID of notification
            val notifyManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val importance = NotificationManager.IMPORTANCE_HIGH
            var mChannel = notifyManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID)
            if (mChannel == null) {
                mChannel = NotificationChannel(
                    NOTIFICATION_CHANNEL_ID,
                    NOTIFICATION_CHANNEL_ID, importance
                )
                mChannel.enableVibration(true)
                mChannel.vibrationPattern = longArrayOf(
                    100,
                    200,
                    300,
                    400,
                    500,
                    400,
                    300,
                    200,
                    400
                )
                notifyManager.createNotificationChannel(mChannel)
            }
            val builder: NotificationCompat.Builder =
                NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
            val pendingIntent: PendingIntent =
                PendingIntent.getActivity(context, 0, mIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            builder.setContentTitle(getString(R.string.app_name))
                .setCategory(Notification.CATEGORY_SERVICE)
                .setSmallIcon(R.drawable.ic_ganso) // required
                .setContentText(message)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_ganso))
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent)
                .setVibrate(longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400))
            val notification: Notification = builder.build()
            notifyManager.notify(NOTIFY_ID, notification)
            startForeground(1, notification)
        } else {
            pendingIntent =
                PendingIntent.getActivity(context, 1, mIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            notification = NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_ganso)
                .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.ic_ganso))
                .setSound(soundUri)
                .setAutoCancel(true)
                .setContentTitle(getString(R.string.app_name))
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentText(message).build()
            notificationManager!!.notify(NOTIFICATION_ID, notification)
        }
    }
}