package com.u4u.universityforuskt.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.SummaryBase
import kotlinx.android.synthetic.main.layout_notes_summary.view.*


class SummaryItemAdapter(
    private val notes: List<SummaryBase>
) : RecyclerView.Adapter<SummaryItemAdapter.ViewHolderData>() {
    private var mParent: ViewGroup? = null
    private val parentView: ViewGroup? = null

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): ViewHolderData {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_notes_summary, parentView, false)
        mParent = parent
        return ViewHolderData(view)
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolderData, position: Int) {
        holder.putData(notes[position])
    }

    override fun getItemCount(): Int {
        return notes.size
    }


    inner class ViewHolderData(@NonNull itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun putData(note: SummaryBase) {
            itemView.summarySemester.text = note.name.toString()
            itemView.semesterSummaryRecycler.layoutManager = LinearLayoutManager(mParent?.context)
            itemView.semesterSummaryRecycler.adapter = note.adapter
        }

    }

}