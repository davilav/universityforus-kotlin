package com.u4u.universityforuskt.ui.reminder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.local.db.reminder.Reminder
import com.u4u.universityforuskt.ui.adapters.ReminderAdapter
import kotlinx.android.synthetic.main.fragment_reminder.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

class ReminderFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private val viewModel: ReminderViewModel by viewModel()
    private var data: MutableList<Reminder>? = null
    private lateinit var adapter: ReminderAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_reminder, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setOnClickListener()
        reminderRecyclerViewSubjects.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        val mList = mutableListOf<Reminder>()
        mList.add(Reminder(0, 1, "my tittle", Calendar.getInstance(), "calculo", "comprar pan xdd"))
        mList.add(Reminder(0, 1, "my tittle", Calendar.getInstance(), "calculo", "comprar pan xdd"))
        mList.add(Reminder(0, 1, "my tittle", Calendar.getInstance(), "calculo", "comprar pan xdd"))
        mList.add(Reminder(0, 1, "my tittle", Calendar.getInstance(), "calculo", "comprar pan xdd"))
        data = mList
        adapter = ReminderAdapter(data!!)
        reminderRecyclerViewSubjects.adapter = adapter
        adapter.notifyDataSetChanged()

    }

    private fun setOnClickListener() {
        reminderFloatingActionMenu.setOnClickListener {
            data!!.add(
                Reminder(
                    0,
                    1,
                    "my tittle",
                    Calendar.getInstance(),
                    "calculo",
                    "comprar pan xdd"
                )
            )
            adapter.notifyDataSetChanged()
        }
    }
}