package com.u4u.universityforuskt.ui.calculator

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.NotesRequest
import com.u4u.universityforuskt.ui.adapters.NotesCalculatorAdapter
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.android.synthetic.main.fragment_calculator.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class CalculatorFragment : Fragment() {
    private val viewModel: CalculatorViewModel by viewModel()
    private lateinit var adapter: NotesCalculatorAdapter
    private var itemTouchHelper: ItemTouchHelper? = null
    private lateinit var data: MutableList<NotesRequest>
    private var result: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_calculator, container, false)
    }

    private fun setOnClickListener() {
        calculatorFloatingActionMenu.setOnClickListener {
            addNotes()
        }

    }

    private fun addNotes() {
        val note = NotesRequest(1, "description", 1, 100, 1, 0.0)
        data.add(note)
        refresh()
        var x: Double = 0.toDouble()
        adapter.notifyItemInserted(data.size)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        setOnClickListener()
        if (!Singleton.isOnline) {
            subjectSearchViewFinderTxt.visibility = View.GONE
        }
        init()
        super.onActivityCreated(savedInstanceState)
    }

    private fun init() {
        val note = NotesRequest(1, "description", 1, 100, 1, 0.0)
        val subData = mutableListOf<NotesRequest>()
        subData.add(note)
        data = subData
        result = requireArguments().getInt("result")
        adapter = NotesCalculatorAdapter(data)
        if (result == 0) {
            calculator.text = ""
            calculatorHistory.text = getString(R.string.PAPA_calculator)
            adapter.iconTxt = " "

        }
        calculatorRecyclerViewSubjects.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        calculatorRecyclerViewSubjects.adapter = adapter
        itemTouchHelper = ItemTouchHelper(viewModel.SwipeToDelete(adapter))
        itemTouchHelper!!.attachToRecyclerView(calculatorRecyclerViewSubjects)
        adapter.userLiveData.observe(viewLifecycleOwner, Observer { response ->
            refresh()
        })
    }

    private fun refresh() {
        if (result == 0) {
            var x: Double = 0.toDouble()
            var y = 0
            for (i in data) {
                val m = i.percent
                val n = i.value
                x += (n * m).toDouble()
                y += i.percent
            }
            x /= y
            calculatorTextViewTotal.text = String.format("%.2f", x)
            calculatorTextViewTotalPercent.text = y.toString()

        } else {
            var x: Double = 0.toDouble()
            var y = 0
            for (i in data) {
                val m = i.percent.toDouble() / 100
                val n = i.value
                x += (n * m).toDouble()
                y += i.percent
            }
            calculatorTextViewTotal.text = String.format("%.2f", x)
            calculatorTextViewTotalPercent.text = y.toString()
        }

    }

}