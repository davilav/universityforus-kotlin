package com.u4u.universityforuskt.ui.subject

import android.os.Handler
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.local.db.note.Note
import com.u4u.universityforuskt.data.models.NotesRequest
import com.u4u.universityforuskt.data.repository.subjects.SubjectRepositoryImpl
import com.u4u.universityforuskt.ui.adapters.NotesAdapter
import com.u4u.universityforuskt.utils.Constants
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.android.synthetic.main.layout_notes.view.*
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
class SubjectViewModel(
    private val repository: SubjectRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {
    private val _dataResponseGetNotes: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseGetNotes: LiveData<Pair<Boolean, Any?>> get() = _dataResponseGetNotes

    private val _dataResponseDeleteNote: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseDeleteNote: LiveData<Pair<Boolean, Any?>> get() = _dataResponseDeleteNote

    private val _dataResponseSetNotes: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseSetNotes: LiveData<Pair<Boolean, Any?>> get() = _dataResponseSetNotes

    private val _dataResponseAddNotes: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseAddNotes: LiveData<Pair<Boolean, Any?>> get() = _dataResponseAddNotes

    fun getNotes(subject_id: Int) {
        viewModelScope.launch {
            val notes = isOfflineNotesEmpty()
            if (!notes.isNullOrEmpty() && !Singleton.isOnline) {
                _dataResponseGetNotes.value = Pair(true, notes)
            } else if (Singleton.isOnline) {
                val response = repository.getNotes(Singleton.token, Singleton.user_id, subject_id)
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        val data = response.data as List<NotesRequest>
                        if (data.isEmpty()) {
                            _dataResponseGetNotes.value = Pair(false, "no se encontraron Notas")
                        } else {
                            setOfflineNotes(data)
                            _dataResponseGetNotes.value = Pair(true, data)
                        }
                    }
                    Result.Status.ERROR -> {
                        _dataResponseGetNotes.value = Pair(false, "no se Notas")
                    }
                }
            } else {
                _dataResponseGetNotes.value = Pair(false, "no se Notas")
            }
        }
    }

    private fun deleteNotes(note_id: Int) {
        viewModelScope.launch {
            val response = repository.deleteNotes(Singleton.token, note_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseDeleteNote.value = Pair(true, ":D")
                }
                Result.Status.ERROR -> {
                    _dataResponseDeleteNote.value = Pair(false, "no se Notas")
                }
            }

        }
    }

    fun setNotes(i: Int, adapter: NotesAdapter, recyclerView: RecyclerView) {
        viewModelScope.launch {
            val itemView: View = recyclerView.getChildAt(i)
            if (!itemView.layoutNotesNote.text.isNullOrBlank() && !itemView.layoutNotesName.text.isNullOrBlank() && !itemView.layoutNotesPercentage.text.isNullOrBlank()){
                val notesRequest = NotesRequest(
                    0,
                    itemView.layoutNotesName.text.toString(),
                    adapter.academicResponses[i].group,
                    itemView.layoutNotesPercentage.text.toString().toInt(),
                    Singleton.user_id,
                    itemView.layoutNotesNote.text.toString().toDouble()
                )
                val response = repository.setNotes(
                    Singleton.token,
                    adapter.academicResponses[i].id,
                    notesRequest
                )
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        _dataResponseSetNotes.value = Pair(true, response.data)
                    }
                    Result.Status.ERROR -> {
                        _dataResponseSetNotes.value = Pair(false, response.data)
                    }
                }
            }
        }
    }

    fun addNotes(group_id: Int) {
        viewModelScope.launch {
            val notesRequest = NotesRequest(0, "_-_-", group_id, 1, Singleton.user_id, 1.0)
            val response = repository.addNotes(Singleton.token, notesRequest)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    _dataResponseAddNotes.value = Pair(true, response.data)
                }
                Result.Status.ERROR -> {
                    _dataResponseAddNotes.value = Pair(false, response.data)
                }
            }
        }
    }

    inner class SwipeToDelete(val adapter: NotesAdapter) :
        ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ): Boolean {
            TODO("Not yet implemented")
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            if (adapter.academicResponses.size <= 1) {
                _dataResponseDeleteNote.value = Pair(false, "No se puede eliminar esta nota")
            } else {
                val pos = viewHolder.adapterPosition
                val id = adapter.academicResponses[pos].id
                adapter.deleteItem(viewHolder, pos)
                Handler().postDelayed({
                    if (adapter.isRemoved) {
                        deleteNotes(id)
                    }
                }, Constants.SNACK_TIME_OUT.toLong())

            }
        }
    }

    private suspend fun setOfflineNotes(events: List<NotesRequest>) {
        if (databaseImpl.getNoteDao().getNotes().isNullOrEmpty()) {
            for (event in events) {
                databaseImpl.getNoteDao().insertNote(
                    Note(
                        event.id,
                        event.description,
                        event.group,
                        event.percent,
                        event.student,
                        event.value
                    )
                )
            }
        } else {
            for (event in events) {
                databaseImpl.getNoteDao().updateNote(
                    Note(
                        event.id,
                        event.description,
                        event.group,
                        event.percent,
                        event.student,
                        event.value
                    )
                )
            }
        }

    }

    fun wakeUp() {
        viewModelScope.launch {

        }
    }


    private suspend fun isOfflineNotesEmpty(): List<NotesRequest>? {
        if (databaseImpl.getNoteDao().getNotes().isNotEmpty()) {
            val eventDB = databaseImpl.getNoteDao().getNotes()
            val myEvents = mutableListOf<NotesRequest>()
            for (event in eventDB) {
                myEvents.add(
                    NotesRequest(
                        event.id,
                        event.description as String,
                        event.noteGroup as Int,
                        event.percent as Int,
                        event.student as Int,
                        event.value as Double
                    )
                )
            }
            return myEvents
        }
        return null
    }

}