
package com.u4u.universityforuskt.ui.profile

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Academic
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.Summary
import com.u4u.universityforuskt.data.models.SummaryBase
import com.u4u.universityforuskt.ui.adapters.SummaryItemAdapter
import com.u4u.universityforuskt.ui.adapters.SummaryNoteAdapter
import com.u4u.universityforuskt.ui.dialogs.SelectOptionDialog
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.android.synthetic.main.layout_alert_dialog.*
import kotlinx.android.synthetic.main.layout_change_carreer.*
import kotlinx.android.synthetic.main.profile_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("UNCHECKED_CAST")
class ProfileFragment : Fragment() {
    private val viewModel: ProfileViewModel by viewModel()
    private val parentView: ViewGroup? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        academicHistoryLayout.toggle()
        val dialogView: View = layoutInflater.inflate(R.layout.layout_change_carreer, parentView)
        val subjectDialog = Dialog(requireContext())
        var careerId = 0
        var facultyId = 0
        subjectDialog.setContentView(dialogView)
        textViewEditProfileName.text = Singleton.first_name + " " + Singleton.last_name
        textViewProfileCareer.text = Singleton.career_name
        viewModel.getAcademicHistory()
        viewModel.subjectsTaken()
        setOnClickListener()
        if (!Singleton.isOnline) {
            searchViewFinderTxt.visibility = View.GONE
        }
        viewModel.dataResponseAcademicHistory.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    textViewEditProfileTotalCreditsNumber.text =
                        (response.second as List<Academic>)[0].credits_number.toString()
                    textViewEditProfileCreditsNumber.text =
                        (response.second as List<Academic>)[0].credits_semester.toString()
                    textViewEditProfileViewedCreditsNumber.text =
                        (response.second as List<Academic>)[0].completed_credits.toString()
                    textViewEditProfileTotalAverage.text =
                        (response.second as List<Academic>)[0].average.toString()
                }
                false -> {
                }
            }
        })
        viewModel.dataResponseLogout.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
                }
                false -> {
                }
            }
        })
        viewModel.dataResponseCareer.observe(viewLifecycleOwner, Observer { response ->
            val text = Gson().toJson(response.second)
            when (response.first) {
                true -> {
                    subjectDialog.changeCareerLayoutTxt.setOnClickListener {
                        SelectOptionDialog.newInstance(
                            text, object : SelectOptionDialog.OnListenerInterface {
                                override fun optionSelect(data: Data?) {
                                    if (data != null) {
                                        careerId = data.id
                                        subjectDialog.changeCareerLayoutTxt.setText(data.name)
                                    }

                                }

                            }
                            , "Carrera").show(childFragmentManager, "")
                    }
                }
                false -> {
                }
            }}
        )

        viewModel.dataResponseDeleteHistory.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    viewModel.deleteNotes()
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseDeleteNote.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    viewModel.deleteSubjects()
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseDeleteSubjects.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    viewModel.updateStudent(careerId,facultyId,0)
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseUpdateStudent.observe(viewLifecycleOwner,Observer { response ->
            when (response.first) {
                true -> {
                    findNavController().navigate(R.id.action_profileFragment_to_loginFragment)
                }
                false -> {
                }
            }
        })

        viewModel.dataResponseFaculty.observe(viewLifecycleOwner, Observer { response ->
            val text = Gson().toJson(response.second)
            when (response.first) {
                true -> {
                    subjectDialog.changeCareerFacultyTxt.setOnClickListener {
                        SelectOptionDialog.newInstance(
                            text,object : SelectOptionDialog.OnListenerInterface{
                                override fun optionSelect(data: Data?) {
                                    if (data != null) {
                                        facultyId = data.id
                                        subjectDialog.changeCareerFacultyTxt.setText(data.name)
                                        subjectDialog.changeCareerLayoutTxt.isEnabled = true
                                        viewModel.getCareer(data.id)
                                    }

                                }

                            }
                            , "Facultad").show(childFragmentManager, "")
                    }
                    subjectDialog.show()
                    subjectDialog.dialogOkButtonProfile.setOnClickListener {
                        viewModel.deleteHistory()
                        subjectDialog.dismiss()
                    }
                    subjectDialog.dialogCancelButtonProfile.setOnClickListener { subjectDialog.dismiss() }
                }
                false -> {
                }
            }
        }
        )

        viewModel.dataResponseSubjectsTaken.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    val list = response.second as List<Summary>
                    val secondList: MutableList<String> = mutableListOf()
                    val thirdList: MutableList<MutableList<Summary>> = mutableListOf()
                    val fourthList: MutableList<SummaryBase> = mutableListOf()
                    for (i in list) {
                        secondList.add(i.year.toString() + "-" + i.semester.toString())
                    }
                    secondList.distinct()
                    secondList.sortDescending()

                    for (i in secondList) {
                        val innerList: MutableList<Summary> = mutableListOf()
                        for (e in list) {
                            if (i == e.year.toString() + "-" + e.semester.toString()) {
                                innerList.add(e)
                            }
                        }
                        thirdList.add(innerList)
                    }

                    for (i in 0 until thirdList.size) {
                        val adapter = SummaryNoteAdapter(thirdList[i])
                        val base = SummaryBase(secondList[i], adapter)
                        fourthList.add(base)
                    }
                    profileSummaryRecycler.layoutManager = LinearLayoutManager(requireContext())
                    val adapter = SummaryItemAdapter(fourthList)
                    profileSummaryRecycler.adapter = adapter
                }
                false -> {
                }
            }
        })
    }


    private fun setOnClickListener() {
        textViewEditProfilePAPACalculator.setOnClickListener {
            val id = 0
            val bundle = bundleOf("result" to id)
            findNavController().navigate(R.id.action_profileFragment_to_calculatorFragment, bundle)
        }
        textViewEditProfileNotesCalculator.setOnClickListener {
            val id = 1
            val bundle = bundleOf("result" to id)
            findNavController().navigate(R.id.action_profileFragment_to_calculatorFragment, bundle)
        }
        textViewEditProfileAcademicHistory.setOnClickListener {
            academicHistoryLayout.toggle()
        }
        textViewEditProfileResume.setOnClickListener {
            profileLayoutResume.toggle()
        }
        searchViewFinderTxt.setOnClickListener { findNavController().navigate(R.id.action_profileFragment_to_finderFragment4) }
        textViewEditProfileChangeCareer.setOnClickListener {
            if (Singleton.isOnline) {
                val mView: View = layoutInflater.inflate(R.layout.layout_alert_dialog, parentView)
                val toChangeCareerDialog = Dialog(requireContext())
                toChangeCareerDialog.setContentView(mView)
                toChangeCareerDialog.textViewMessage.setText(R.string.warningChangeCareer)
                toChangeCareerDialog.textViewTittle.setText(R.string.change_career)
                toChangeCareerDialog.show()
                toChangeCareerDialog.buttonCancel.setOnClickListener { toChangeCareerDialog.dismiss() }
                toChangeCareerDialog.buttonSubmit.setOnClickListener {
                    toChangeCareerDialog.dismiss()
                    viewModel.getFaculty()
                }
            } else {
                Snackbar.make(
                    requireView(),
                    "No es Posible hacer esto sin conexion",
                    Snackbar.LENGTH_SHORT
                ).show()
            }


        }
        textViewEditProfileLogout.setOnClickListener {
            val mView: View = layoutInflater.inflate(R.layout.layout_alert_dialog, parentView)
            val toChangeCareerDialog = Dialog(requireContext())
            toChangeCareerDialog.setContentView(mView)
            toChangeCareerDialog.textViewMessage.setText(R.string.ExitMessage)
            toChangeCareerDialog.textViewTittle.setText(R.string.ExitTittle)
            toChangeCareerDialog.show()
            toChangeCareerDialog.buttonCancel.setOnClickListener { toChangeCareerDialog.dismiss() }
            toChangeCareerDialog.buttonSubmit.setOnClickListener {
                viewModel.logout()
                toChangeCareerDialog.dismiss()
            }
        }
    }


}
