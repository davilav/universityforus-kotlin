package com.u4u.universityforuskt.ui.notification

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.core.content.ContextCompat


class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val service1 = Intent(p0, NotificationService::class.java)
        service1.data = Uri.parse("custom://" + System.currentTimeMillis())
        ContextCompat.startForegroundService(p0 as Context, service1)
        Log.d("WALKIRIA", " ALARM RECEIVED!!!")
    }
}