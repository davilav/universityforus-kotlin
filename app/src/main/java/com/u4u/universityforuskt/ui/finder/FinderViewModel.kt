package com.u4u.universityforuskt.ui.finder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.models.Comment
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.TeachersAverageResponseItem
import com.u4u.universityforuskt.data.models.TeachersCommentsResponse
import com.u4u.universityforuskt.data.repository.finder.FinderRepositoryImpl
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.launch

@Suppress("UNCHECKED_CAST")
class FinderViewModel(
    private val repository: FinderRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponseFinder: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseFinder: LiveData<Pair<Boolean, Any?>> get() = _dataResponseFinder

    private val _dataResponseComments: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseComments: LiveData<Pair<Boolean, Any?>> get() = _dataResponseComments

    private val _dataResponseAllComments: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseAllComments: LiveData<Pair<Boolean, Any?>> get() = _dataResponseAllComments

    private val _dataResponseAverages: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseAverages: LiveData<Pair<Boolean, Any?>> get() = _dataResponseAverages

    private val _dataResponseItemDisplay: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseItemDisplay: LiveData<Pair<Boolean, Any?>> get() = _dataResponseItemDisplay

    var page: Int = 1

    private var comments:List<Comment> = mutableListOf()

    fun getFindTeachers(keyword: String){
        viewModelScope.launch {
            val response = repository.getFindTeachers(Singleton.token, keyword)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as List<Data>
                    if (data.isEmpty()) {
                        _dataResponseFinder.value = Pair(false, R.string.TeacherError)
                    } else {
                        _dataResponseFinder.value = Pair(true, data)
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseFinder.value = Pair(false, R.string.TeacherError)
                }
            }
        }
    }
   fun getTeacherComments( teacher_id: Int, page: Int = 1){
        viewModelScope.launch {
            val response = repository.getTeacherComments(Singleton.token, teacher_id, page)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as TeachersCommentsResponse
                    if (data.results.isEmpty()) {
                        _dataResponseComments.value = Pair(false, R.string.CommentError)
                    } else {
                        _dataResponseComments.value = Pair(true, data.results)
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseComments.value = Pair(false, R.string.CommentError)
                }
            }
        }
    }

    fun getAllTeacherComments( teacher_id: Int){
        viewModelScope.launch {
            val response = repository.getTeacherComments(Singleton.token, teacher_id, page)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as TeachersCommentsResponse
                    if (data.results.isEmpty()) {
                        _dataResponseAllComments.value = Pair(false, R.string.CommentError)
                    } else {
                        if (comments.size < data.count) {
                            comments = if (page != 1) {
                                comments + data.results
                            } else {
                                data.results
                            }
                            _dataResponseAllComments.value = Pair(true, comments)
                        }
                        page += 1
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseAllComments.value = Pair(false, R.string.CommentError)
                }
            }
        }
    }

    fun isLastItemDisplaying(recyclerView: RecyclerView){
        if (recyclerView.adapter!!.itemCount != 0) {
            val lastVisibleItemPosition =
                (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastCompletelyVisibleItemPosition()
            if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.adapter!!
                    .itemCount - 1
            ) {
                _dataResponseItemDisplay.value = Pair(first = true, second = true)
            }
        }
        _dataResponseItemDisplay.value = Pair(first = false, second = false)
    }
    fun resetComments(){
        comments = mutableListOf()
        page = 1
    }

    fun getTeacherAverages( teacher_id: Int){
        viewModelScope.launch {
            val response = repository.getTeacherAverages(Singleton.token, teacher_id)
            when (response.status) {
                Result.Status.SUCCESS -> {
                    val data = response.data as List<TeachersAverageResponseItem>
                    if (data.isEmpty()) {
                        _dataResponseAverages.value = Pair(false, "no se encontraron promedios")
                    } else {
                        _dataResponseAverages.value = Pair(true, data[0])
                    }
                }
                Result.Status.ERROR -> {
                    _dataResponseAverages.value = Pair(false, "no se encontraron promedios")
                }
            }
        }
    }
}