package com.u4u.universityforuskt.ui.dialogs


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.ui.adapters.SelectOptionAdapter
import kotlinx.android.synthetic.main.layout_option_dialog.*
import kotlinx.android.synthetic.main.layout_option_dialog.view.*

//TODO(setTittle)
class SelectOptionDialog(name: String) : DialogFragment(), SelectOptionAdapter.OnAdapterListener {
    private var listData: ArrayList<Data>? = null
    private var mAdapter: SelectOptionAdapter? = null
    private var name: String? = null

    companion object {
        private const val LIST_DATA = "list_data"
        private var mListener: OnListenerInterface? = null
        fun newInstance(
            listData: String?,
            callback: OnListenerInterface,
            Mname: String
        ): SelectOptionDialog {
            mListener = callback
            val frag = SelectOptionDialog(Mname)
            val args = Bundle()
            args.putString(LIST_DATA, listData)
            frag.arguments = args
            return frag
        }
    }
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.Theme_MaterialComponents_Dialog_Alert)
        if (arguments != null) {
            listData = Gson().fromJson(
                arguments?.getString(LIST_DATA),
                object : TypeToken<List<Data?>?>() {}.type
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false
        return inflater.inflate(R.layout.layout_option_dialog, container)
    }

    override fun onViewCreated(
        @NonNull view: View,
        @Nullable savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView: RecyclerView = view.recyclerOption
        view.spinnerTitle.text = name
        mAdapter = SelectOptionAdapter(listData, this)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                DividerItemDecoration.HORIZONTAL
            )
        )
        recyclerView.adapter = mAdapter
        this.searchBox.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                mAdapter!!.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mAdapter!!.filter?.filter(newText)
                return false
            }
        })
        view.textCancel.setOnClickListener { dismiss() }
    }

    override fun itemSelect(data: Data) {
        dismiss()
        mListener?.optionSelect(data)
    }

    interface OnListenerInterface {
        fun optionSelect(data: Data?)
    }

}