package com.u4u.universityforuskt.ui.splash

import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.data.repository.splash.SplashRepositoryImpl
import com.u4u.universityforuskt.utils.Constants
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.launch
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class SplashViewModel(
    private val repository: SplashRepositoryImpl,
    private val databaseImpl: LocalDataBase
) : ViewModel() {

    private val _dataResponse: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponse: LiveData<Pair<Boolean, Any?>> get() = _dataResponse

    private val _dataResponseOnline: MutableLiveData<Pair<Boolean, Any?>> = MutableLiveData()
    val dataResponseOnline: LiveData<Pair<Boolean, Any?>> get() = _dataResponseOnline

    private suspend fun isLogged(): Boolean {
        val user = databaseImpl.getStudentDao().getStudent()
        val history = databaseImpl.getHistoryDao().getHistory()
        return if (user.isNotEmpty()) {
            if (user[0].isLogged == 1) {
                if (!history.isNullOrEmpty()) {
                    repository.fillData(user[0], history[0].creditsSemester as Int)
                } else {
                    repository.fillData(user[0], 0)
                }
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    fun getData(context: Context) {
        viewModelScope.launch {
            if (Singleton.isOnline) {
                val response = repository.getSplashUniversity()
                when (response.status) {
                    Result.Status.SUCCESS -> {
                        val isLogged: Boolean = isLogged()
                        _dataResponse.value = Pair(true, isLogged)
                    }
                    Result.Status.ERROR -> {
                        val dataResult = response.data as? String
                        _dataResponse.value = Pair(false, dataResult)
                    }
                }

            } else {
                val isLogged: Boolean = isLogged()
                if (isLogged) {
                    _dataResponse.value = Pair(true, isLogged)
                } else {
                    _dataResponse.value = Pair(false, "nothing to do bro")
                }
            }

        }
    }


    private fun hasNetworkAvailable(context: Context): Boolean {
        val service = Context.CONNECTIVITY_SERVICE
        val manager = context.getSystemService(service) as ConnectivityManager?
        val network = manager?.activeNetworkInfo
        Log.d("hello", "hasNetworkAvailable: ${(network != null)}")
        return (network?.isConnected) ?: false
    }


    fun isOnline(context: Context) {
        AsyncTask.execute {
            try {
                if (hasNetworkAvailable(context)) {
                    try {
                        val connection =
                            URL(Constants.REACHABILITY_SERVER).openConnection() as HttpURLConnection
                        connection.setRequestProperty("User-Agent", "Test")
                        connection.setRequestProperty("Connection", "close")
                        connection.connectTimeout = 1500 // configurable
                        connection.connect()
                        Log.d("hello", "hasInternetConnected: ${(connection.responseCode == 200)}")
                        val myResponse = (connection.responseCode in 200..299)
                        Singleton.isOnline = myResponse
                    } catch (e: IOException) {
                        Log.e("hello", "Error checking internet connection", e)
                    }
                } else {
                    Log.w("hello", "No network available!")
                }
                Log.d("hello", "hasInternetConnected: false")
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        _dataResponseOnline.value = Pair(true, "hello")
    }
}