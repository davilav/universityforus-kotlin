package com.u4u.universityforuskt.ui.adapters

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import kotlinx.android.synthetic.main.schedule_list.view.*


class ScheduleAdapter(private var groups: List<Data>) :
    RecyclerView.Adapter<ScheduleAdapter.ViewHolderData>() {

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): ViewHolderData {
        val view  = View.inflate(parent.context, R.layout.schedule_list, null)
        return ViewHolderData(view)
    }

    override fun onBindViewHolder(
        @NonNull holder: ViewHolderData,
        position: Int
    ) {
        holder.putData(groups[position])
    }

    override fun getItemCount(): Int {
        return groups.size
    }

    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun putData(data: Data) {
            itemView.textViewLayoutSubjectDayTxt.text = data.day
            itemView.textViewLayoutSubjectStartTxt.text = data.time_start + ":00"
            itemView.textViewLayoutSubjectEndTxt.text = data.time_end + ":00"
        }

    }

}
