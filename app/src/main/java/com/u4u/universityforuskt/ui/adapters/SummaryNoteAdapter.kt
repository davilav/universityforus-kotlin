package com.u4u.universityforuskt.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.u4u.universityforuskt.data.models.Summary
import kotlinx.android.synthetic.main.layout_comment.view.*
import java.lang.String


class SummaryNoteAdapter(
    private val notes: List<Summary>
) : RecyclerView.Adapter<SummaryNoteAdapter.ViewHolderData>() {

    private val parentView: ViewGroup? = null

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): ViewHolderData {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(com.u4u.universityforuskt.R.layout.layout_note, parentView, false)
        return ViewHolderData(view)
    }

    override fun onBindViewHolder(
        @NonNull holder: ViewHolderData,
        position: Int
    ) {
        holder.putData(notes[position])
    }

    override fun getItemCount(): Int {
        return notes.size
    }


    inner class ViewHolderData(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun putData(note: Summary) {
            itemView.textViewLayoutSubjectFinder.text = note.subject
            val x = " " + String.valueOf(note.value)
            itemView.textViewLayoutNoteFinder.text = x
        }

    }

}