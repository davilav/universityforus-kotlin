package com.u4u.universityforuskt.ui.register

import android.app.DatePickerDialog
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.u4u.universityforuskt.R
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.ui.dialogs.DatePickerFragment
import com.u4u.universityforuskt.ui.dialogs.SelectOptionDialog
import com.u4u.universityforuskt.utils.Utils
import kotlinx.android.synthetic.main.register_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegisterFragment : Fragment() {

    private val viewModel: RegisterViewModel by viewModel()
    private val fields: ArrayList<Any> = ArrayList()//TODO(put this in the MV)
    private val fields2: ArrayList<Any> = ArrayList()//TODO(put this in the MV)
    private var universityId: Int = 0
    private var facultyId: Int = 0
    private var careerId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.register_fragment, container, false)
    }

    private fun setOnClickListener() {
        Utils().closeKeyboard(requireView(), requireContext())
        switchBeginner.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->
            if (b) {
                fields[5] = 0
            } else {
                fields[5] = 1
            }
        }
        registerMaleGender.setOnClickListener {
            Utils().closeKeyboard(requireView(), requireContext())
            fields[4] = "male"
            viewModel.validateFirst(fields)
            registerMaleGender.setColorFilter(
                Color.argb(255, 171, 209, 255)
            )
            registerFemaleGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
            registerOtherGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
        }
        registerFemaleGender.setOnClickListener {
            Utils().closeKeyboard(requireView(),requireContext())
            fields[4] = "female"
            viewModel.validateFirst(fields)
            registerFemaleGender.setColorFilter(
                Color.argb(255, 171, 209, 255)
            )
            registerMaleGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
            registerOtherGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
        }
        registerOtherGender.setOnClickListener {
            Utils().closeKeyboard(requireView(),requireContext())
            fields[4] = "other"
            viewModel.validateFirst(fields)
            registerOtherGender.setColorFilter(
                Color.argb(255, 171, 209, 255)
            )
            registerFemaleGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
            registerMaleGender.setColorFilter(
                Color.argb(
                    Color.alpha(Color.WHITE),
                    Color.red(Color.WHITE),
                    Color.green(Color.WHITE),
                    Color.blue(Color.WHITE)
                )
            )
        }
        registerLayoutDateTxt.setOnClickListener {
            viewModel.validateFirst(fields)
            registerLayoutEmailTxt.clearFocus()
            registerLayoutFirstNameTxt.clearFocus()
            registerLayoutLastNameTxt.clearFocus()
            Utils().closeKeyboard(requireView(),requireContext())
            showDatePickerDialog()
        }
        nextButton.setOnClickListener {
            viewModel.validateFirst(fields)
            viewModel.showHide(register)
            viewModel.showHide(register2)
        }
        registerButton.setOnClickListener {
            viewModel.validateSecond(fields2)
            viewModel.validateFirst(fields)
            viewModel.setRegisterUser(
                fields[3].toString(),
                fields2[5] as Int,
                0,
                fields2[4] as Int,
                1,
                fields[4].toString(),
                fields[5] as Int,
                fields2[3] as Int,
                fields[2].toString(),
                fields[0].toString(),
                fields[1].toString(),
                fields2[1].toString(),
                fields2[2].toString(),
                fields2[0].toString()
            )
        }
        registerLayoutUniversityTxt.setOnClickListener {
            registerLayoutFacultyTxt.isEnabled = false
            registerLayoutFacultyTxt.setText("")
            fields2[4] = false
            fields2[5] = false
            registerLayoutCareerTxt.isEnabled = false
            registerLayoutCareerTxt.setText("")
            viewModel.getUniversityData()
            textInputLayoutUserTxt.clearFocus()
            registerLayoutPasswordTxt.clearFocus()
            registerLayoutPassword2Txt.clearFocus()
        }
        registerLayoutFacultyTxt.setOnClickListener {
            textInputLayoutUserTxt.clearFocus()
            registerLayoutPasswordTxt.clearFocus()
            registerLayoutPassword2Txt.clearFocus()
            viewModel.getFacultyData(universityId)
        }
        registerLayoutCareerTxt.setOnClickListener {
            viewModel.getCareerData(facultyId)
            viewModel.validateSecond(fields2)
        }
        textInputLayoutUserTxt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateSecond(fields2)
            registerLayoutPasswordTxt.clearFocus()
            registerLayoutPassword2Txt.clearFocus()
            if (!b) viewModel.validateUsername(
                textInputLayoutUserTxt.text.toString()
            )
        }
        registerLayoutFirstNameTxt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateFirst(fields)
            if (!b) viewModel.validateFirstName(
                registerLayoutFirstNameTxt.text.toString()
            )
        }
        registerLayoutLastNameTxt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateFirst(fields)
            if (!b) viewModel.validateLastName(
                registerLayoutLastNameTxt.text.toString()
            )
        }
        registerLayoutEmailTxt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateFirst(fields)
            if (!b) viewModel.validateEmail(
                registerLayoutEmailTxt.text.toString()
            )
        }
        registerLayoutPasswordTxt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateSecond(fields2)
            textInputLayoutUserTxt.clearFocus()
            registerLayoutPassword2Txt.clearFocus()
            if (!b) viewModel.validatePassword1(
                registerLayoutPasswordTxt.text.toString()
            )
        }
        registerLayoutPassword2Txt.onFocusChangeListener = View.OnFocusChangeListener { _, b ->
            viewModel.validateSecond(fields2)
            textInputLayoutUserTxt.clearFocus()
            registerLayoutPasswordTxt.clearFocus()
            if (!b) viewModel.validatePassword2(
                registerLayoutPassword2Txt.text.toString(),
                registerLayoutPasswordTxt.text.toString()
            )
        }
    }

    private fun showDatePickerDialog() {
        val newFragment =
            DatePickerFragment().newInstance(DatePickerDialog.OnDateSetListener { _, year, month, day ->
                val selectedDate = day.toString() + "/" + (month + 1) + "/" + year
                registerLayoutDateTxt.setText(selectedDate)
                fields[3] = selectedDate
            })

        newFragment.show(childFragmentManager, "datePicker")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.paintGenders(registerMaleGender, registerFemaleGender, registerOtherGender)
        initVars()
        setOnClickListener()
        //TODO(remove focus)}
        //TODO(problem with focus)
        viewModel.dataResponseUser.observe(viewLifecycleOwner, Observer { response ->
            textInputLayoutUserTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            when (response.first) {
                111 -> {
                    textInputLayoutUserTxt.error = resources.getString(R.string._111_)
                    //TODO("request focus not work")
                }
                112 -> {
                    textInputLayoutUserTxt.error = resources.getString(R.string._112_)
                    //TODO("request focus not work")
                }
                107 -> {
                    textInputLayoutUserTxt.error = resources.getString(R.string._107_)
                    //TODO("request focus not work")
                }
                200 -> {
                    fields2[0] = textInputLayoutUserTxt.text.toString()
                }
            }
        })

        viewModel.dataResponseFirstName.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                116 -> {
                    registerLayoutFirstNameTxt.error = resources.getString(R.string._116_)
                    response.second?.let { fields.set(0, it) }
                    //TODO("request focus not work")
                }
                200 -> {
                    fields[0] = registerLayoutFirstNameTxt.text.toString()
                    //TODO("request focus not work")
                }
            }
        })

        viewModel.dataResponseLastName.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                117 -> {
                    registerLayoutLastNameTxt.error = resources.getString(R.string._117_)
                    response.second?.let { fields.set(1, it) }
                }
                200 -> {
                    fields[1] = registerLayoutLastNameTxt.text.toString()
                }
            }
        })

        viewModel.dataResponseEmail.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                118 -> {
                    registerLayoutEmailTxt.error = resources.getString(R.string._118_)
                    response.second?.let { fields.set(2, it) }
                    //TODO("request focus not work")
                }
                101 -> {
                    registerLayoutEmailTxt.error = resources.getString(R.string._101_)
                    response.second?.let { fields.set(2, it) }
                    //TODO("request focus not work")
                }
                200 -> {
                    fields[2] = registerLayoutEmailTxt.text.toString()
                    //TODO("request focus not work")
                }

            }
        })

        viewModel.dataResponseValidateFirst.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                100 -> {
                    nextButton.isEnabled = false
                }
                200 -> {
                    nextButton.isEnabled = true
                }
            }
        })

        viewModel.dataResponseValidateSecond.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                100 -> {
                    registerButton.isEnabled = false
                }
                200 -> {
                    registerButton.isEnabled = true
                }
            }
        })

        viewModel.dataResponsePassword1.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                104 -> {
                    registerLayoutPasswordTxt.error = resources.getString(R.string._104_)
                }
                103 -> {
                    registerLayoutPasswordTxt.error = resources.getString(R.string._103_)
                }
                120 -> {
                    registerLayoutPasswordTxt.error = resources.getString(R.string._120_)
                }
                200 -> {
                    fields2[1] = registerLayoutPasswordTxt.text.toString()
                }

            }
        })

        viewModel.dataResponsePassword2.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                100 -> {
                    registerLayoutPassword2Txt.error = resources.getString(R.string._100_)
                }
                120 -> {
                    registerLayoutPassword2Txt.error = resources.getString(R.string._120_)
                }
                200 -> {
                    fields2[2] = registerLayoutPassword2Txt.text.toString()
                }

            }
        })

        viewModel.dataResponseListUniversities.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    Utils().closeKeyboard(requireView(), requireContext())
                    SelectOptionDialog.newInstance(response.second,
                        object : SelectOptionDialog.OnListenerInterface {
                            override fun optionSelect(data: Data?) {
                                universityId = data!!.id
                                fields2[3] = data.id
                                registerLayoutFacultyTxt.isEnabled = true
                                registerLayoutUniversityTxt.setText(data.name)
                            }
                        }, "Universidad"
                    ).show(childFragmentManager, "")
                    viewModel.validateSecond(fields2)
                }
                false -> {

                }
            }
        })

        viewModel.dataResponseListFaculties.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    Utils().closeKeyboard(requireView(), requireContext())
                    SelectOptionDialog.newInstance(response.second,
                        object : SelectOptionDialog.OnListenerInterface {
                            override fun optionSelect(data: Data?) {
                                facultyId = data!!.id
                                fields2[4] = data.id
                                registerLayoutCareerTxt.isEnabled = true
                                registerLayoutFacultyTxt.setText(data.name)
                            }
                        }, "Facultad"
                    ).show(childFragmentManager, "")
                    viewModel.validateSecond(fields2)
                }
                false -> {

                }
            }
        })

        viewModel.dataResponseListCareers.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    Utils().closeKeyboard(requireView(), requireContext())
                    SelectOptionDialog.newInstance(response.second,
                        object : SelectOptionDialog.OnListenerInterface {
                            override fun optionSelect(data: Data?) {
                                careerId = data!!.id
                                fields2[5] = data.id
                                registerLayoutCareerTxt.setText(data.name)
                            }
                        }, "Carrera"
                    ).show(childFragmentManager, "")
                    viewModel.validateSecond(fields2)
                }
                false -> {

                }
            }
        })

        viewModel.dataResponseRegister.observe(viewLifecycleOwner, Observer { response ->
            when (response.first) {
                true -> {
                    findNavController().navigate(R.id.action_registerFragment_to_loginFragment2)
                }
                false -> {

                }
            }
        })
    }

    private fun initVars() {
        fields.add(false)
        fields.add(false)
        fields.add(false)
        fields.add(false)
        fields.add(false)
        fields.add(0)
        fields2.add(false)
        fields2.add(false)
        fields2.add(false)
        fields2.add(false)
        fields2.add(false)
        fields2.add(false)

    }
}