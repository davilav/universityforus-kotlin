package com.u4u.universityforuskt.data.local.db.note

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Note(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "description") val description: String?,
    @ColumnInfo(name = "noteGroup") val noteGroup: Int?,
    @ColumnInfo(name = "percent") val percent: Int?,
    @ColumnInfo(name = "student") val student: Int?,
    @ColumnInfo(name = "value") val value: Double?
)
