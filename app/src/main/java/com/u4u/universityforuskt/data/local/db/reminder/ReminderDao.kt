package com.u4u.universityforuskt.data.local.db.reminder

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ReminderDao {
    @Insert
    suspend fun insertReminder(reminder: Reminder)

    @Update
    suspend fun updateReminder(reminder: Reminder)

    @Query("SELECT  * FROM reminder WHERE subject_id = :subjectId")
    suspend fun getReminder(subjectId: Long): List<Reminder>

    @Query("DELETE FROM reminder WHERE subject_id = :subjectId")
    suspend fun deleteFromTable(subjectId: Long)
}