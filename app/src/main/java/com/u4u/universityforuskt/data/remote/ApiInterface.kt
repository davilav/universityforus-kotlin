package com.u4u.universityforuskt.data.remote

import com.u4u.universityforuskt.data.models.*
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @POST("api/v1/login/")
    fun login(@Body loginRequest: LoginRequest?): Call<LoginResponse> //User Login

    @POST("api/v1/students/")
    fun register(@Body register: Register?): Call<Register>//User Register

    @GET("api/v1/university/")
    fun getUniversity(): Call<List<Data>> //Get the universities

    @GET("api/v1/careers/")
    fun getCareers(@Header("Authorization") token: String?): Call<List<Data>> // Get the careers

    @GET("api/v1/faculties/")
    fun getFaculties(@Header("Authorization") token: String?): Call<List<Data>>// Get the faculties

    @GET("api/v1/university/{university_id}/faculties/")
    fun getFaculty(@Path("university_id") university_id: Int): Call<List<Data>>// Get the faculties by university

    @GET("api/v1/faculty/{faculty_id}/careers/")
    fun getCareer(@Path("faculty_id") faculty_id: Int): Call<List<Data>> // Get career by faculty

    @GET("api/v1/career/{career_id}/subjects/")
    fun getSubjects(
        @Header("Authorization") token: String?,
        @Path("career_id") career_id: Int
    ): Call<List<Data>> // Get subjects by career

    @GET("api/v1/subject/{subject_id}/groups/")
    fun getGroups(
        @Header("Authorization") token: String?,
        @Path("subject_id") subject_id: Int
    ): Call<List<Data>>// Get the groups by subject

    @POST("api/v1/groupstudent/")
    fun setStudentGroups(
        @Header("Authorization") token: String,
        @Body groupRequest: Group
    ): Call<Group>// Set the Groups

    @PUT("api/v1/students/{user_id}/")
    fun updateStudent(
        @Header("Authorization") token: String,
        @Path("user_id") user_id: Int,
        @Body updateRequest: UpdateRequest
    ): Call<Register>// Update the student

    @GET("api/v1/student/{user_id}/groups/")
    fun getStudentGroups(
        @Header("Authorization") token: String?,
        @Path("user_id") user_id: Int
    ): Call<List<Data>> //Get the student groups

    @GET("api/v1/subject/{subject_id}/groups/{group_id}/")
    fun getSchedule(
        @Header("Authorization") token: String?,
        @Path("subject_id") subject_id: Int,
        @Path("group_id") group_id: Int
    ): Call<List<Data>> //Get the Student Schedule

    @GET("api/v1/teachers/")
    fun getFindTeachers(
        @Header("Authorization") token: String?,
        @Query("q") keyword: String?
    ): Call<List<Data>>// Find the teachers

    @GET("api/v1/teacher/{teacher_id}/averages/")
    fun getTeacherAverages(
        @Header("Authorization") token: String?,
        @Path("teacher_id") teacher_id: Int
    ): Call<List<TeachersAverageResponseItem>>//Get the teachers average

    @GET("api/v1/teacher/{teacher_id}/references/")
    fun getTeacherComments(
        @Header("Authorization") token: String?,
        @Path("teacher_id") teacher_id: Int,
        @Query("page") page: Int
    ): Call<TeachersCommentsResponse>

    @POST("api/v1/references/")
    fun addNewComment(
        @Header("Authorization") token: String?,
        @Body commentRequest: CommentRequest
    ): Call<Comment>// Add new Comment

    @GET("api/v1/teacher/{teacher_id}/subjects/")
    fun getTeacherSubjects(
        @Header("Authorization") token: String?,
        @Path("teacher_id") teacher_id: Int
    ): Call<List<Data>>// Get the subjects by teacher

    @POST("api/v1/logout/")
    fun logout(@Header("Authorization") token: String?): Call<Data>// :)

    @GET("api/v1/student/{student_id}/history/")
    fun getAcademicHistory(
        @Header("Authorization") token: String?,
        @Path("student_id") student_id: Int
    ): Call<List<Academic>>// Get the academic history of the student

    @POST("/api/v1/history/")
    fun setAcademicHistory(
        @Header("Authorization") token: String?,
        @Body academicRequest: AcademicItem
    ): Call<Academic>// Set the academic history of the student

    @POST("/api/v1/notestudentbysubject/")
    fun setNotes(
        @Header("Authorization") token: String?,
        @Body notesRequest: NotesRequest
    ): Call<NotesRequest>//Set the student notes

    @GET("/api/v1/student/{student_id}/subjects/")
    fun getStudentSubjects(
        @Header("Authorization") token: String?,
        @Path("student_id") user_id: Int
    ): Call<ArrayList<SubjectResponse>>// Get the student subjects

    @GET("/api/v1/student/{student_id}/group/notes/{group_id}/")
    fun getNotes(
        @Header("Authorization") token: String?,
        @Path("student_id") user_id: Int,
        @Path("group_id") group_id: Int
    ): Call<List<NotesRequest>>//Get the student notes

    @DELETE("/api/v1/notestudentbysubject/{note_id}/")
    fun deleteNotes(
        @Header("Authorization") token: String?,
        @Path("note_id") note_id: Int
    ): Call<Data>

    @PUT("/api/v1/notestudentbysubject/{note_id}/")
    fun putNotes(@Header("Authorization") token: String?,
        @Path("note_id") note_id: Int,
        @Body notesRequest: NotesRequest?
    ): Call<NotesRequest>

    @DELETE("/api/v1/groupstudent/{note_id}/")
    fun deleteSubject(
        @Header("Authorization") token: String?,
        @Path("note_id") note_id: Int
    ): Call<Data>

    @DELETE("api/v1/history/{user_id}/")
    fun deleteHistory(
        @Header("Authorization") token: String?,
        @Path("user_id") user_id: Int
    ): Call<Data>

    @GET("api/v1/student/{user_id}/groupstudent/")
    fun deleteAllSubjects(
        @Header("Authorization") token: String?,
        @Path("user_id") user_id: Int
    ): Call<List<DeleteNotesResponse>>

    @GET("api/v1/student/{user_id}/notestudentbysubject/")
    fun deleteAllNotes(
        @Header("Authorization") token: String?,
        @Path("user_id") user_id: Int
    ): Call<List<DeleteNotesResponse>>

    @GET("/api/v1/student/{user_id}/subjectstaken/")
    fun getSubjectsTaken(
        @Header("Authorization") token: String?,
        @Path("user_id") user_id: Int
    ): Call<List<Summary>>

}