package com.u4u.universityforuskt.data.models

import com.u4u.universityforuskt.ui.adapters.SummaryNoteAdapter

data class SummaryBase(
    val name: String,
    val adapter: SummaryNoteAdapter
)