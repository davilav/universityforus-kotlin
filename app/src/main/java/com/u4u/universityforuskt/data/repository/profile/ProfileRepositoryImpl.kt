package com.u4u.universityforuskt.data.repository.profile

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.*
import com.u4u.universityforuskt.data.remote.ApiInterface
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class ProfileRepositoryImpl(
    private val apiClient: ApiInterface
) : ProfileRepository {

    override suspend fun getAcademicHistory(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Academic>> = apiClient.getAcademicHistory(token, user_id)
            call.enqueue(object : Callback<List<Academic>> {

                override fun onResponse(
                    call: Call<List<Academic>>,
                    response: Response<List<Academic>>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<Academic>>, t: Throwable) {
                    cancellableContinuation.resume(Result.success(t.message))
                }

            })
        }

    override suspend fun logout(token: String): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Data> = apiClient.logout(token)
            call.enqueue(object : Callback<Data> {
                override fun onResponse(
                    call: Call<Data>,
                    response: Response<Data>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }
                override fun onFailure(call: Call<Data>, t: Throwable) {
                    cancellableContinuation.resume(Result.success(t.message))
                }

            })
        }

    override suspend fun getCareer(faculty_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getCareer(faculty_id)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun getFaculty(university_id:Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getFaculty(university_id)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun deleteHistory(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Data> = apiClient.deleteHistory(token, user_id)
            call.enqueue(object : Callback<Data> {

                override fun onResponse(call: Call<Data>, response: Response<Data>) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<Data>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun deleteAllSubjects(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<DeleteNotesResponse>> = apiClient.deleteAllSubjects(token, user_id)
            call.enqueue(object : Callback<List<DeleteNotesResponse>> {

                override fun onResponse(
                    call: Call<List<DeleteNotesResponse>>,
                    response: Response<List<DeleteNotesResponse>>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<DeleteNotesResponse>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun deleteAllNotes(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<DeleteNotesResponse>> = apiClient.deleteAllNotes(token, user_id)
            call.enqueue(object : Callback<List<DeleteNotesResponse>> {

                override fun onResponse(
                    call: Call<List<DeleteNotesResponse>>,
                    response: Response<List<DeleteNotesResponse>>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<DeleteNotesResponse>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun updateStudent(
        token: String,
        user_id: Int,
        updateRequest: UpdateRequest
    ): Result<Any> = suspendCancellableCoroutine { cancellableContinuation ->
        val call: Call<Register> = apiClient.updateStudent(token,user_id,updateRequest)
        call.enqueue(object : Callback<Register> {
            override fun onResponse(call: Call<Register>, response: Response<Register>) {
                if (response.isSuccessful) {
                    cancellableContinuation.resume(Result.success(response.body()))
                } else {
                    cancellableContinuation.resume(Result.error(response.errorBody()))
                }
            }

            override fun onFailure(call: Call<Register>, t: Throwable) {
                cancellableContinuation.resume(Result.error(t.message))
            }
        })
    }

    override suspend fun subjectsTaken(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Summary>> = apiClient.getSubjectsTaken(token, user_id)
            call.enqueue(object : Callback<List<Summary>> {
                override fun onResponse(
                    call: Call<List<Summary>>,
                    response: Response<List<Summary>>
                ) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Summary>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

}