package com.u4u.universityforuskt.data.repository.profile

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.UpdateRequest

interface ProfileRepository {
    suspend fun getAcademicHistory(token: String, user_id: Int): Result<Any>
    suspend fun logout(token: String): Result<Any>
    suspend fun getCareer(faculty_id: Int): Result<Any>
    suspend fun getFaculty(university_id: Int): Result<Any>
    suspend fun deleteHistory(token: String, user_id: Int): Result<Any>
    suspend fun deleteAllSubjects(token: String, user_id: Int): Result<Any>
    suspend fun deleteAllNotes(token: String, user_id: Int): Result<Any>
    suspend fun updateStudent(
        token: String,
        user_id: Int,
        updateRequest: UpdateRequest
    ): Result<Any>

    suspend fun subjectsTaken(token: String, user_id: Int): Result<Any>
}