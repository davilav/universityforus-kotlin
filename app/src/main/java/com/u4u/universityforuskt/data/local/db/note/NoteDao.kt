package com.u4u.universityforuskt.data.local.db.note

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface NoteDao {

    @Query("DELETE FROM Note WHERE id = :noteId")
    suspend fun deleteNote(noteId: Int)

    @Insert
    suspend fun insertNote(note: Note)

    @Update
    suspend fun updateNote(note: Note)

    @Query("SELECT * FROM Note WHERE id = :noteId")
    suspend fun getNotesById(noteId: Int): List<Note>

    @Query("SELECT  * FROM Note")
    suspend fun getNotes(): List<Note>
}