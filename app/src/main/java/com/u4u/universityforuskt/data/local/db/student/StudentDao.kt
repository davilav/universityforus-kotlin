package com.u4u.universityforuskt.data.local.db.student

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface StudentDao {

    @Insert
    suspend fun insertStudent(student: Student)

    @Update
    suspend fun updateStudent(student: Student)

    @Query("SELECT  * FROM student")
    suspend fun getStudent(): List<Student>
}