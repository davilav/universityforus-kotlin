package com.u4u.universityforuskt.data.local.db.summary

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Summary(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "semester") val semester: Int?,
    @ColumnInfo(name = "student") val student: Int?,
    @ColumnInfo(name = "subject") val subject: String?,
    @ColumnInfo(name = "value") val value: Double?,
    @ColumnInfo(name = "year") val year: Int?
)