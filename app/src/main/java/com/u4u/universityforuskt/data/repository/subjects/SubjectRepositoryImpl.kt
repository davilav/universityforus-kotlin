package com.u4u.universityforuskt.data.repository.subjects

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.NotesRequest
import com.u4u.universityforuskt.data.remote.ApiInterface
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class SubjectRepositoryImpl(
    private val apiClient: ApiInterface
) : SubjectRepository {

    override suspend fun getNotes(token: String, user_id: Int, teacher_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<NotesRequest>> = apiClient.getNotes(token, user_id, teacher_id)
            call.enqueue(object : Callback<List<NotesRequest>> {
                override fun onResponse(
                    call: Call<List<NotesRequest>>,
                    response: Response<List<NotesRequest>>
                ) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<NotesRequest>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

    override suspend fun deleteNotes(token: String, note_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Data> = apiClient.deleteNotes(token, note_id)
            call.enqueue(object : Callback<Data> {
                override fun onResponse(
                    call: Call<Data>,
                    response: Response<Data>
                ) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<Data>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

    override suspend fun setNotes(token: String?,note_id: Int,notesRequest: NotesRequest): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<NotesRequest> = apiClient.putNotes(token,note_id,notesRequest)
            call.enqueue(object : Callback<NotesRequest> {

                override fun onResponse(call: Call<NotesRequest>, response: Response<NotesRequest>) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<NotesRequest>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun addNotes(token: String, notesRequest: NotesRequest): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<NotesRequest> = apiClient.setNotes(token, notesRequest)
            call.enqueue(object : Callback<NotesRequest> {

                override fun onResponse(
                    call: Call<NotesRequest>,
                    response: Response<NotesRequest>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<NotesRequest>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }
}