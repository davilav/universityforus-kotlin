package com.u4u.universityforuskt.data.repository.splash

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.SharedPreferenceHelper
import com.u4u.universityforuskt.data.local.db.student.Student
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.remote.ApiInterface
import com.u4u.universityforuskt.utils.Constants
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class SplashRepositoryImpl(
    private val apiClient: ApiInterface,
    private val preferencesHelper: SharedPreferenceHelper
) : SplashRepository {

    override suspend fun getSplashUniversity(): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getUniversity()
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    preferencesHelper.saveList(Constants.KEY_UNIVERSITY, response.body() as List<*>)
                    cancellableContinuation.resume(Result.success(null))
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override fun fillData(student: Student, credits_Semester: Int) {
        Singleton.token = student.token.toString()
        Singleton.career = student.career!!
        Singleton.email = student.email.toString()
        Singleton.faculty = student.faculty!!
        Singleton.first_name = student.firstName.toString()
        Singleton.last_name = student.lastName.toString()
        Singleton.university = student.university!!
        Singleton.user_id = student.id!!
        Singleton.username = student.userName.toString()
        Singleton.last_login = " "
        Singleton.last_name = " "
        Singleton.credits_semester = credits_Semester
    }


}