package com.u4u.universityforuskt.data.models

data class LoginRequest(
    val password: String,
    val username: String
)