package com.u4u.universityforuskt.data.repository.register

import com.u4u.universityforuskt.data.models.Register
import com.u4u.universityforuskt.data.Result

interface RegisterRepository {
    suspend fun setRegisterUser(register:Register?):Result<Any>
    suspend fun getUniversityData():Result<Any>
    suspend fun getFacultyData(university_id:Int):Result<Any>
    suspend fun getCareerData(faculty_id:Int):Result<Any>
}