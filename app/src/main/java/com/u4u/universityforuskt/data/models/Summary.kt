package com.u4u.universityforuskt.data.models

data class Summary(
    val id: Int,
    val semester: Int,
    val student: Int,
    val subject: String,
    val value: Double,
    val year: Int
)