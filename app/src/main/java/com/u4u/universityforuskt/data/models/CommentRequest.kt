package com.u4u.universityforuskt.data.models

data class CommentRequest(
    val amenity_qualification: Double,
    val classes_qualification: Double,
    val description: String,
    val experience_qualification: Double,
    val explication_qualification: Double,
    val final_note: Double,
    val semester: String,
    val student: Int,
    val student_qualification: Double,
    val subject: Int,
    val teacher: Int,
    val year: Int
)