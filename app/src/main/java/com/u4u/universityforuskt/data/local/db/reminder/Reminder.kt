package com.u4u.universityforuskt.data.local.db.reminder

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Reminder(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "notUsed") val notUsed: Long,
    @ColumnInfo(name = "subject_id") val subjectId: Long?,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "time") val time: Calendar?,
    @ColumnInfo(name = "subject_name") val subjectName: String?,
    @ColumnInfo(name = "description") val description: String?
)