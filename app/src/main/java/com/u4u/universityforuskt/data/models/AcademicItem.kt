package com.u4u.universityforuskt.data.models

data class AcademicItem(
    val average: Double,
    val completed_credits: Int,
    val student: Int
)
