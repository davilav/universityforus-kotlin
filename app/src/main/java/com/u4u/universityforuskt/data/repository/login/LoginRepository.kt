package com.u4u.universityforuskt.data.repository.login

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.LoginRequest

interface LoginRepository {
    suspend fun setLoginUser(loginRequest: LoginRequest?):Result<Any>
}