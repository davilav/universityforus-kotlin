package com.u4u.universityforuskt.data.repository.subjects

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.NotesRequest

interface SubjectRepository {
    suspend fun getNotes(token: String, user_id: Int, teacher_id: Int): Result<Any>
    suspend fun deleteNotes(token: String, note_id: Int): Result<Any>
    suspend fun setNotes(token: String?, note_id: Int, notesRequest: NotesRequest): Result<Any>
    suspend fun addNotes(token: String, notesRequest: NotesRequest): Result<Any>
}