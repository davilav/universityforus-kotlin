package com.u4u.universityforuskt.data.repository.home

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.AcademicItem
import com.u4u.universityforuskt.data.models.Group
import com.u4u.universityforuskt.data.models.NotesRequest
import com.u4u.universityforuskt.data.models.UpdateRequest

interface HomeRepository {
    suspend fun getEvents(token: String, user_id: Int): Result<Any>
    suspend fun getSubjects(token: String, career_id: Int): Result<Any>
    suspend fun getGroups(token: String, subject_id: Int): Result<Any>
    suspend fun getSchedule(token: String, subject_id: Int, groupId: Int): Result<Any>
    suspend fun setGroups(token: String, group: Group): Result<Any>
    suspend fun updateStudent(
        token: String,
        user_id: Int,
        updateRequest: UpdateRequest
    ): Result<Any>

    suspend fun deleteSubject(token: String, note_id: Int): Result<Any>
    suspend fun setAcademicHistory(token: String, academic: AcademicItem): Result<Any>
    suspend fun getSplashCareer(token: String): Result<Any>
    suspend fun setNotes(token: String, notesRequest: NotesRequest): Result<Any>
}