package com.u4u.universityforuskt.data.local.db.history

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface HistoryDao {
    @Insert
    suspend fun insertHistory(student: History)

    @Update
    suspend fun updateHistory(student: History)

    @Query("SELECT  * FROM history")
    suspend fun getHistory(): List<History>
}