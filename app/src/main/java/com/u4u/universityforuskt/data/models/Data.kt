package com.u4u.universityforuskt.data.models

data class Data (
    val id:Int,
    val name:String,
    val code:String,
    val university:Int,
    val credits_number:Int,
    val classes_id:Int,
    val faculty: Int,
    val day: String,
    val time_start: String,
    val time_end: String,
    val teacher_name: String,
    val subject_name: String,
    val Success: String,
    val group_id: Int,
    val group_student: String,
    val subject_id: Int,
    val group_number: String,
    val building_name: String,
    val classroom_Name: String
)