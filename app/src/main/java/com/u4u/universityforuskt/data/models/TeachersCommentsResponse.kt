package com.u4u.universityforuskt.data.models

data class TeachersCommentsResponse(
    val count: Int,
    val next: Any,
    val previous: Any,
    val results: List<Comment>
)