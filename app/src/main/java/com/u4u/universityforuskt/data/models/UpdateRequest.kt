package com.u4u.universityforuskt.data.models

data class UpdateRequest(
    val career: Int,
    val faculty:Int,
    val credits_semester: Int,
    val first_login: Int,
    val new_student: Int
)