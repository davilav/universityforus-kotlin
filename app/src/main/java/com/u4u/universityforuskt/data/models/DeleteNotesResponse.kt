package com.u4u.universityforuskt.data.models

data class DeleteNotesResponse(
    val group: Any,
    val student: Any
)