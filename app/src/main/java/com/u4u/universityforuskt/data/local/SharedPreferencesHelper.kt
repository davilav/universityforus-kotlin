package com.u4u.universityforuskt.data.local

import android.app.Application
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.google.gson.Gson

class SharedPreferenceHelper(
    application: Application
) {

    private val sharedPreferences: SharedPreferences = application.getSharedPreferences(PREFS_NAME, MODE_PRIVATE)

    fun save(KEY_NAME: String, value: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(KEY_NAME, value)
        editor.apply()
    }

    fun save(KEY_NAME: String, value: Int) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putInt(KEY_NAME, value)
        editor.apply()
    }

    fun save(KEY_NAME: String, value: Boolean) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putBoolean(KEY_NAME, value)
        editor.apply()
    }

    fun saveList(KEY_NAME: String, value: List<*>) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(KEY_NAME, Gson().toJson(value))
        editor.apply()
    }

    fun getValueString(KEY_NAME: String): String? = sharedPreferences.getString(KEY_NAME, null)

    fun getValueInt(KEY_NAME: String): Int = sharedPreferences.getInt(KEY_NAME, 0)

    fun getValueBoolean(KEY_NAME: String, defaultValue: Boolean): Boolean = sharedPreferences.getBoolean(KEY_NAME, defaultValue)

    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.remove(KEY_NAME)
        editor.apply()
    }

    companion object {
        const val PREFS_NAME = "PREFS_NAME"
    }
}