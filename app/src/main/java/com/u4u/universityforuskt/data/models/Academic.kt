package com.u4u.universityforuskt.data.models

data class Academic(
    val average: Double,
    val completed_credits: Int,
    val credits_number: Int,
    val credits_semester: Int,
    val id: Int,
    val progress: Double,
    val Error:String
)