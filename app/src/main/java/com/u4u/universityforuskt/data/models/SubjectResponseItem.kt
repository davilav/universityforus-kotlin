package com.u4u.universityforuskt.data.models

data class SubjectResponseItem(
    val credits_number: Int,
    val group: Int,
    val id: Int,
    val note_acumulated: Double,
    val percent_acumulated: Int,
    val positive_note: Double,
    val subject: String
)