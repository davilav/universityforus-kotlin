package com.u4u.universityforuskt.data.local.db.schedule

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Schedule(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "notUsed") val notUsed: Long,
    @ColumnInfo(name = "id") val id: Long?,
    @ColumnInfo(name = "class_id") val classId: Long?,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "startTime") val startTime: Calendar?,
    @ColumnInfo(name = "endTime") val endTime: Calendar?,
    @ColumnInfo(name = "location") val location: String?,
    @ColumnInfo(name = "subject_name") val subjectName: String?,
    @ColumnInfo(name = "color") val color: Int?,
    @ColumnInfo(name = "credits") val credits: Int?,
    @ColumnInfo(name = "isAllDay") val isAllDay: Boolean?,
    @ColumnInfo(name = "isCanceled") val isCanceled: Boolean?,
    @ColumnInfo(name = "building_name") val building_name: String?,
    @ColumnInfo(name = "classroom_name") val classroom_Name: String?

)