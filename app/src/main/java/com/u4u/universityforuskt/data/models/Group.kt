package com.u4u.universityforuskt.data.models

data class Group(
    val group: Int,
    val status: Int,
    val student: Int
){
    constructor(id: Int,student: Int,group: Int,status: Int) : this(group, status, student)
}