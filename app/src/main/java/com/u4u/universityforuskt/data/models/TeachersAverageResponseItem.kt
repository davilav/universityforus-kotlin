package com.u4u.universityforuskt.data.models

data class TeachersAverageResponseItem(
    val amenity_average: Double,
    val classes_average: Double,
    val experience_average: Double,
    val explication_average: Double,
    val id: Int,
    val name: String,
    val note_average: Double,
    val student_average: Double
)