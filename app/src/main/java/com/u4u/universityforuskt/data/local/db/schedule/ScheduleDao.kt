package com.u4u.universityforuskt.data.local.db.schedule

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ScheduleDao {


    @Insert
    suspend fun insertEvents(events: Schedule)

    @Update
    suspend fun updateEvents(events: Schedule)

    @Query("SELECT  * FROM schedule")
    suspend fun getEvents(): List<Schedule>

    @Query("DELETE FROM schedule")
    suspend fun nukeTable()

}