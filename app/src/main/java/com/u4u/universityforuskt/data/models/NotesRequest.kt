package com.u4u.universityforuskt.data.models

data class NotesRequest(
    val id:Int = 0,
    var description: String,
    val group: Int,
    var percent: Int,
    val student: Int,
    var value: Double
)