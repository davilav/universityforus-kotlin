package com.u4u.universityforuskt.data.models

data class LoginResponse(
    val birth_date: String,
    val career: Int,
    val credits_semester: Int,
    val email: String,
    val faculty: Int,
    val first_login: Int,
    val first_name: String,
    val gender: String,
    val last_login: String,
    val last_name: String,
    val new_student: Int,
    val token: String,
    val university: Int,
    val user_id: Int,
    val username: String
)