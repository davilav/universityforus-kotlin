package com.u4u.universityforuskt.data.models

data class Register(
    val birth_date: String,
    val career: Int,
    val credits_semester: Int,
    val faculty: Int,
    val first_login: Int,
    val gender: String,
    val new_student: Int,
    val university: Int,
    val user: User
)