package com.u4u.universityforuskt.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.u4u.universityforuskt.data.local.db.history.History
import com.u4u.universityforuskt.data.local.db.history.HistoryDao
import com.u4u.universityforuskt.data.local.db.note.Note
import com.u4u.universityforuskt.data.local.db.note.NoteDao
import com.u4u.universityforuskt.data.local.db.reminder.Reminder
import com.u4u.universityforuskt.data.local.db.reminder.ReminderDao
import com.u4u.universityforuskt.data.local.db.schedule.Schedule
import com.u4u.universityforuskt.data.local.db.schedule.ScheduleDao
import com.u4u.universityforuskt.data.local.db.student.Student
import com.u4u.universityforuskt.data.local.db.student.StudentDao
import com.u4u.universityforuskt.data.local.db.summary.Summary
import com.u4u.universityforuskt.data.local.db.summary.SummaryDao

@Database(
    entities = [Student::class, History::class, Note::class, Schedule::class, Summary::class, Reminder::class],
    version = 20
)
@TypeConverters(Converters::class)
abstract class LocalDataBase : RoomDatabase() {
    abstract fun getStudentDao(): StudentDao
    abstract fun getHistoryDao(): HistoryDao
    abstract fun getNoteDao(): NoteDao
    abstract fun getScheduleDao(): ScheduleDao
    abstract fun getSummaryDao(): SummaryDao
    abstract fun getReminderDao(): ReminderDao
}