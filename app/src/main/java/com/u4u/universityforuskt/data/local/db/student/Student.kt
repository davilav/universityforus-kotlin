package com.u4u.universityforuskt.data.local.db.student

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Student(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "token") val token: String?,
    @ColumnInfo(name = "first_name") val firstName: String?,
    @ColumnInfo(name = "last_name") val lastName: String?,
    @ColumnInfo(name = "user_name") val userName: String?,
    @ColumnInfo(name = "career") val career: Int?,
    @ColumnInfo(name = "faculty") val faculty: Int?,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = "university") val university: Int?,
    @ColumnInfo(name = "isLogged") val isLogged: Int?,
    @ColumnInfo(name = "careerName") val careerName: String?
)