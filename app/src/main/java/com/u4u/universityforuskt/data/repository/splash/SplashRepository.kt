package com.u4u.universityforuskt.data.repository.splash
import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.db.student.Student

interface SplashRepository {
    suspend fun getSplashUniversity(): Result<Any>
    fun fillData(student: Student, credits_Semester: Int)

}