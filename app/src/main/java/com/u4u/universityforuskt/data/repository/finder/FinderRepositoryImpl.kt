package com.u4u.universityforuskt.data.repository.finder

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.TeachersAverageResponseItem
import com.u4u.universityforuskt.data.models.TeachersCommentsResponse
import com.u4u.universityforuskt.data.remote.ApiInterface
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class FinderRepositoryImpl(
    private val apiClient: ApiInterface
) : FinderRepository {

    override suspend fun getFindTeachers(token: String, keyword: String): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getFindTeachers(token, keyword)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }

    override suspend fun getTeacherComments(
        token: String,
        teacher_id: Int,
        page: Int
    ): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<TeachersCommentsResponse> =
                apiClient.getTeacherComments(token, teacher_id, page)
            call.enqueue(object : Callback<TeachersCommentsResponse> {
                override fun onResponse(
                    call: Call<TeachersCommentsResponse>,
                    response: Response<TeachersCommentsResponse?>
                ) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<TeachersCommentsResponse?>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

    override suspend fun getTeacherAverages(token: String, teacher_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<TeachersAverageResponseItem>> =
                apiClient.getTeacherAverages(token, teacher_id)
            call.enqueue(object : Callback<List<TeachersAverageResponseItem>> {

                override fun onResponse(
                    call: Call<List<TeachersAverageResponseItem>>,
                    response: Response<List<TeachersAverageResponseItem>>
                ) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(
                    call: Call<List<TeachersAverageResponseItem>>,
                    t: Throwable
                ) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }


}