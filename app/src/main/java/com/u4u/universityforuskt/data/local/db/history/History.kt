package com.u4u.universityforuskt.data.local.db.history

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class History(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "credits_number") val creditsNumber: Int?,
    @ColumnInfo(name = "credits_semester") val creditsSemester: Int?,
    @ColumnInfo(name = "completed_credits") val completedCredits: Int?,
    @ColumnInfo(name = "average") val average: Double?,
    @ColumnInfo(name = "progress") val progress: Double?
)