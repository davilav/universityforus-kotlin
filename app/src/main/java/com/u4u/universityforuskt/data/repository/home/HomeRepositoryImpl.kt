package com.u4u.universityforuskt.data.repository.home

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.*
import com.u4u.universityforuskt.data.remote.ApiInterface
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class HomeRepositoryImpl(
    private val apiClient: ApiInterface
) : HomeRepository {

    override suspend fun getEvents(token: String, user_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getStudentGroups(token, user_id)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

    override suspend fun getSubjects(token: String, career_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getSubjects(token, career_id)
            call.enqueue(object : Callback<List<Data>> {
                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }

    override suspend fun getGroups(token: String, subject_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getGroups(token, subject_id)
            call.enqueue(object : Callback<List<Data>> {
                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }

    override suspend fun getSchedule(token: String, subject_id: Int, groupId: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getSchedule(token, subject_id, groupId)
            call.enqueue(object : Callback<List<Data>> {
                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }

    override suspend fun setGroups(token: String, group: Group): Result<Any> = suspendCancellableCoroutine { cancellableContinuation ->
        val call: Call<Group> = apiClient.setStudentGroups(token,group)
        call.enqueue(object : Callback<Group> {
            override fun onResponse(call: Call<Group>, response: Response<Group>) {
                if (response.isSuccessful) {
                    cancellableContinuation.resume(Result.success(response.body()))
                } else {
                    cancellableContinuation.resume(Result.error(response.errorBody()))
                }
            }

            override fun onFailure(call: Call<Group>, t: Throwable) {
                cancellableContinuation.resume(Result.error(t.message))
            }

        })
    }

    override suspend fun updateStudent(
        token: String,
        user_id: Int,
        updateRequest: UpdateRequest
    ): Result<Any> = suspendCancellableCoroutine { cancellableContinuation ->
        val call: Call<Register> = apiClient.updateStudent(token,user_id,updateRequest)
        call.enqueue(object : Callback<Register> {
            override fun onResponse(call: Call<Register>, response: Response<Register>) {
                if (response.isSuccessful) {
                    cancellableContinuation.resume(Result.success(response.body()))
                } else {
                    cancellableContinuation.resume(Result.error(response.errorBody()))
                }
            }

            override fun onFailure(call: Call<Register>, t: Throwable) {
                cancellableContinuation.resume(Result.error(t.message))
            }
        })
    }

    override suspend fun deleteSubject(token: String, note_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Data> = apiClient.deleteSubject(token, note_id)
            call.enqueue(object : Callback<Data> {
                override fun onResponse(call: Call<Data>, response: Response<Data>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<Data>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun setAcademicHistory(token: String, academic: AcademicItem): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Academic> = apiClient.setAcademicHistory(token, academic)
            call.enqueue(object : Callback<Academic> {
                override fun onResponse(call: Call<Academic>, response: Response<Academic>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<Academic>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun getSplashCareer(token: String): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getCareers(token)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }

    override suspend fun setNotes(token: String, notesRequest: NotesRequest): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<NotesRequest> = apiClient.setNotes(token, notesRequest)
            call.enqueue(object : Callback<NotesRequest> {

                override fun onResponse(
                    call: Call<NotesRequest>,
                    response: Response<NotesRequest>
                ) {
                    cancellableContinuation.resume(Result.success(response.body()))
                }

                override fun onFailure(call: Call<NotesRequest>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }


            })
        }
}