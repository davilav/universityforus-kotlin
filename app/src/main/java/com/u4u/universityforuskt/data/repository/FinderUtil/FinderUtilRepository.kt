package com.u4u.universityforuskt.data.repository.FinderUtil

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.CommentRequest

interface FinderUtilRepository {
    suspend fun getTeacherSubjects(token: String, teacher_id: Int): Result<Any>
    suspend fun addNewComment(token: String, commentRequest: CommentRequest): Result<Any>
}