package com.u4u.universityforuskt.data.local.db.summary

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface SummaryDao {
    @Insert
    suspend fun insertSummary(summary: Summary)

    @Update
    suspend fun updateSummary(summary: Summary)

    @Query("SELECT * FROM Summary ")
    suspend fun getSummary(): List<Summary>
}