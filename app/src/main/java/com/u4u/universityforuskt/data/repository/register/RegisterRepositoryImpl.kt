package com.u4u.universityforuskt.data.repository.register

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.SharedPreferenceHelper
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.models.Register
import com.u4u.universityforuskt.data.remote.ApiInterface
import com.u4u.universityforuskt.utils.Constants
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class RegisterRepositoryImpl(
    private val apiClient: ApiInterface,
    private val preferenceHelper: SharedPreferenceHelper
) : RegisterRepository {

    override suspend fun setRegisterUser(register: Register?): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Register> = apiClient.register(register)
            call.enqueue(object : Callback<Register> {

                override fun onResponse(call: Call<Register>, response: Response<Register>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(null))
                    } else {
                        cancellableContinuation.resume(Result.error(" setRegisterUser-ERROR"))
                    }
                }

                override fun onFailure(call: Call<Register>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }

            })
        }

    override suspend fun getUniversityData(): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val universities = preferenceHelper.getValueString(Constants.KEY_UNIVERSITY)
            if (!universities.isNullOrBlank()) {
                cancellableContinuation.resume(Result.success(universities))
            } else {
                cancellableContinuation.resume(Result.error("getUniversityData-ERROR"))
            }

        }

    override suspend fun getFacultyData(university_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getFaculty(university_id)
            call.enqueue(object : Callback<List<Data>> {

                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(" setRegisterUser-ERROR"))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })

        }

    override suspend fun getCareerData(faculty_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getCareer(faculty_id)
            call.enqueue(object : Callback<List<Data>> {
                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(" setRegisterUser-ERROR"))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })

        }
}