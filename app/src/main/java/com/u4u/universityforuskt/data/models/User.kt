package com.u4u.universityforuskt.data.models

data class User(
    val email: String,
    val first_name: String,
    val last_name: String,
    val password1: String,
    val password2: String,
    val username: String
)