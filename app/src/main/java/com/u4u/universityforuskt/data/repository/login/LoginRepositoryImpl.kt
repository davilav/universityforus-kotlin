package com.u4u.universityforuskt.data.repository.login

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.local.SharedPreferenceHelper
import com.u4u.universityforuskt.data.models.LoginRequest
import com.u4u.universityforuskt.data.models.LoginResponse
import com.u4u.universityforuskt.data.remote.ApiInterface
import com.u4u.universityforuskt.utils.Singleton
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class LoginRepositoryImpl(
    private val apiClient: ApiInterface,
    private val preferencesHelper: SharedPreferenceHelper
) : LoginRepository {

    override suspend fun setLoginUser(loginRequest: LoginRequest?): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<LoginResponse> = apiClient.login(loginRequest)
            call.enqueue(object : Callback<LoginResponse> {

                override fun onResponse(call: Call<LoginResponse>,response: Response<LoginResponse>) {
                    if (response.isSuccessful){
                        response.body()?.let { student ->
                            Singleton.token = "Token " + student.token
                            Singleton.career = student.career
                            Singleton.email = student.email
                            Singleton.faculty = student.faculty
                            Singleton.credits_semester = student.credits_semester
                            Singleton.first_login= student.first_login
                            Singleton.first_name=student.first_name
                            Singleton.last_name=student.last_name
                            Singleton.last_login=student.last_login
                            Singleton.new_student=student.new_student
                            Singleton.university = student.university
                            Singleton.user_id=student.user_id
                            Singleton.username = student.username
                        }
                        cancellableContinuation.resume(Result.success(null))
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }
}