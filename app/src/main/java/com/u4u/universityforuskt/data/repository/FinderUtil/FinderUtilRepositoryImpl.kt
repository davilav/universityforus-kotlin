package com.u4u.universityforuskt.data.repository.FinderUtil

import com.u4u.universityforuskt.data.Result
import com.u4u.universityforuskt.data.models.Comment
import com.u4u.universityforuskt.data.models.CommentRequest
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.data.remote.ApiInterface
import kotlinx.coroutines.suspendCancellableCoroutine
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.resume

class FinderUtilRepositoryImpl(
    private val apiClient: ApiInterface
) : FinderUtilRepository {

    override suspend fun getTeacherSubjects(token: String, teacher_id: Int): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<List<Data>> = apiClient.getTeacherSubjects(token, teacher_id)
            call.enqueue(object : Callback<List<Data>> {
                override fun onResponse(call: Call<List<Data>>, response: Response<List<Data>>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<List<Data>>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }

    override suspend fun addNewComment(token: String, commentRequest: CommentRequest): Result<Any> =
        suspendCancellableCoroutine { cancellableContinuation ->
            val call: Call<Comment> = apiClient.addNewComment(token, commentRequest)
            call.enqueue(object : Callback<Comment> {
                override fun onResponse(call: Call<Comment>, response: Response<Comment>) {
                    if (response.isSuccessful) {
                        cancellableContinuation.resume(Result.success(response.body()))
                    } else {
                        cancellableContinuation.resume(Result.error(response.errorBody()))
                    }
                }

                override fun onFailure(call: Call<Comment>, t: Throwable) {
                    cancellableContinuation.resume(Result.error(t.message))
                }
            })
        }
}
