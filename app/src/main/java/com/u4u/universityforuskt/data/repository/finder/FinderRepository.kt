package com.u4u.universityforuskt.data.repository.finder

import com.u4u.universityforuskt.data.Result

interface FinderRepository {
    suspend fun getFindTeachers(token: String, keyword: String): Result<Any>
    suspend fun getTeacherComments(token: String, teacher_id: Int, page: Int): Result<Any>
    suspend fun getTeacherAverages(token: String, teacher_id: Int): Result<Any>
}