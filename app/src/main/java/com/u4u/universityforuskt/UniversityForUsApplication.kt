package com.u4u.universityforuskt

import android.app.Application
import com.u4u.universityforuskt.di.localModule
import com.u4u.universityforuskt.di.networkModule
import com.u4u.universityforuskt.di.repositoryModule
import com.u4u.universityforuskt.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class UniversityForUsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@UniversityForUsApplication)
            modules(arrayListOf(localModule, networkModule, repositoryModule, viewModelModule))
        }
    }
}