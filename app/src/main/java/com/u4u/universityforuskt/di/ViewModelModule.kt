package com.u4u.universityforuskt.di

import com.u4u.universityforuskt.ui.calculator.CalculatorViewModel
import com.u4u.universityforuskt.ui.finder.FinderViewModel
import com.u4u.universityforuskt.ui.finderutil.FinderUtilViewModel
import com.u4u.universityforuskt.ui.home.HomeViewModel
import com.u4u.universityforuskt.ui.login.LoginViewModel
import com.u4u.universityforuskt.ui.profile.ProfileViewModel
import com.u4u.universityforuskt.ui.register.RegisterViewModel
import com.u4u.universityforuskt.ui.reminder.ReminderViewModel
import com.u4u.universityforuskt.ui.splash.SplashViewModel
import com.u4u.universityforuskt.ui.subject.SubjectViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module{
    viewModel { SplashViewModel(get(), get()) }
    viewModel { LoginViewModel(get(), get()) }
    viewModel { RegisterViewModel(get(), get()) }
    viewModel { HomeViewModel(get(), get()) }
    viewModel { ProfileViewModel(get(), get()) }
    viewModel { FinderViewModel(get(), get()) }
    viewModel { FinderUtilViewModel(get(), get()) }
    viewModel { SubjectViewModel(get(), get()) }
    viewModel { CalculatorViewModel(get(), get()) }
    viewModel { ReminderViewModel(get()) }
}