package com.u4u.universityforuskt.di

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.u4u.universityforuskt.data.local.SharedPreferenceHelper
import com.u4u.universityforuskt.data.local.db.LocalDataBase
import com.u4u.universityforuskt.utils.Constants
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val localModule = module {
    single { SharedPreferenceHelper(androidApplication()) }
    single { provideRoom(androidApplication()) }
}

private const val DATABASE_NAME = Constants.DATABASE_NAME
val MIGRATION_1_2 = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Student ADD COLUMN isLogged INTEGER")
    }
}
val MIGRATION_2_3 = object : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE History(id INTEGER PRIMARY KEY NOT NULL, credits_number INTEGER, credits_semester INTEGER,  completed_credits INTEGER, average DOUBLE, progress DOUBLE)")
    }
}

val MIGRATION_3_4 = object : Migration(3, 4) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Schedule(id LONG PRIMARY KEY NOT NULL, class_id LONG, title LONG,  startTime TIMESTAMP, endTime TIMESTAMP, location STRING, subject_name STRING, color INTEGER, credits INTEGER,isAllDay BOOLEAN,isCanceled BOOLEAN)")
    }
}

val MIGRATION_4_5 = object : Migration(4, 5) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Reminder(id LONG PRIMARY KEY NOT NULL, description STRING, noteGroup INTEGER,  percent INTEGER, student INTEGER, value DOUBLE)")
    }
}
val MIGRATION_5_6 = object : Migration(5, 6) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("DROP TABLE Reminder")
    }
}
val MIGRATION_6_7 = object : Migration(6, 7) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Note(id LONG PRIMARY KEY NOT NULL, description TEXT, noteGroup INTEGER,  percent INTEGER, student INTEGER, value DOUBLE)")
    }
}

val MIGRATION_7_8 = object : Migration(7, 8) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Summary(id LONG PRIMARY KEY NOT NULL, semester INTEGER, student INTEGER,  subject STRING, value DOUBLE,year INTEGER)")
    }
}
val MIGRATION_8_9 = object : Migration(8, 9) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("DROP TABLE Summary")
    }
}
val MIGRATION_9_10 = object : Migration(9, 10) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("DROP TABLE Schedule")
    }
}
val MIGRATION_10_11 = object : Migration(10, 11) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("DROP TABLE Note")
    }
}

val MIGRATION_11_12 = object : Migration(11, 12) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Note(id INTEGER PRIMARY KEY NOT NULL, description TEXT, noteGroup INTEGER,  percent INTEGER, student INTEGER, value REAL)")
    }
}

val MIGRATION_12_13 = object : Migration(12, 13) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Schedule(id BIGINT PRIMARY KEY NOT NULL, class_id INTEGER, title TEXT,  startTime INTEGER, endTime INTEGER, location TEXT, subject_name TEXT, color INTEGER, credits INTEGER,isAllDay INTEGER ,isCanceled INTEGER)")
    }
}

val MIGRATION_13_14 = object : Migration(13, 14) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Summary(id INTEGER PRIMARY KEY NOT NULL, semester INTEGER, student INTEGER,  subject TEXT, value REAL,year INTEGER)")
    }
}

val MIGRATION_14_15 = object : Migration(14, 15) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("DROP TABLE Schedule")
    }
}

val MIGRATION_15_16 = object : Migration(15, 16) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Schedule(notUsed INTEGER PRIMARY KEY NOT NULL,id INTEGER, class_id INTEGER, title TEXT,  startTime INTEGER, endTime INTEGER, location TEXT, subject_name TEXT, color INTEGER, credits INTEGER,isAllDay INTEGER ,isCanceled INTEGER)")
    }
}
val MIGRATION_16_17 = object : Migration(16, 17) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Student ADD COLUMN careerName TEXT")
    }
}
val MIGRATION_17_18 = object : Migration(17, 18) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Schedule ADD COLUMN building_name TEXT")
    }
}
val MIGRATION_18_19 = object : Migration(18, 19) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("ALTER TABLE Schedule ADD COLUMN classroom_name TEXT")
    }
}
val MIGRATION_19_20 = object : Migration(19, 20) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE Reminder(notUsed INTEGER PRIMARY KEY NOT NULL,subject_id INTEGER, title TEXT, time INTEGER, subject_name TEXT,description TEXT)")
    }
}
fun provideRoom(application: Application): LocalDataBase = Room.databaseBuilder(
    application.applicationContext,
    LocalDataBase::class.java,
    DATABASE_NAME
).addMigrations(
    MIGRATION_1_2,
    MIGRATION_2_3,
    MIGRATION_3_4,
    MIGRATION_4_5,
    MIGRATION_5_6,
    MIGRATION_6_7,
    MIGRATION_7_8,
    MIGRATION_8_9,
    MIGRATION_9_10,
    MIGRATION_10_11,
    MIGRATION_11_12,
    MIGRATION_12_13,
    MIGRATION_13_14,
    MIGRATION_14_15,
    MIGRATION_15_16,
    MIGRATION_16_17,
    MIGRATION_17_18,
    MIGRATION_18_19,
    MIGRATION_19_20
).build()