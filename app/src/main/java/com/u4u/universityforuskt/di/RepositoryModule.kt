package com.u4u.universityforuskt.di

import com.u4u.universityforuskt.data.repository.FinderUtil.FinderUtilRepositoryImpl
import com.u4u.universityforuskt.data.repository.finder.FinderRepositoryImpl
import com.u4u.universityforuskt.data.repository.home.HomeRepositoryImpl
import com.u4u.universityforuskt.data.repository.login.LoginRepositoryImpl
import com.u4u.universityforuskt.data.repository.profile.ProfileRepositoryImpl
import com.u4u.universityforuskt.data.repository.register.RegisterRepositoryImpl
import com.u4u.universityforuskt.data.repository.splash.SplashRepositoryImpl
import com.u4u.universityforuskt.data.repository.subjects.SubjectRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module{
    single { SplashRepositoryImpl(get(),get()) }
    single { LoginRepositoryImpl(get(),get()) }
    single { RegisterRepositoryImpl(get(),get()) }
    single { HomeRepositoryImpl(get()) }
    single { ProfileRepositoryImpl(get()) }
    single { FinderRepositoryImpl(get()) }
    single { FinderUtilRepositoryImpl(get()) }
    single { SubjectRepositoryImpl(get()) }
}
