package com.u4u.universityforuskt.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.u4u.universityforuskt.data.models.Data
import com.u4u.universityforuskt.ui.notification.AlarmReceiver
import java.util.*


class Utils {

    fun setAlarm(i: Int, timestamp: Long?, ctx: Context) {
        val alarmManager =
            ctx.getSystemService(ALARM_SERVICE) as AlarmManager
        val alarmIntent = Intent(ctx, AlarmReceiver::class.java)
        val pendingIntent: PendingIntent
        pendingIntent = PendingIntent.getBroadcast(ctx, i, alarmIntent, PendingIntent.FLAG_ONE_SHOT)
        alarmIntent.data = Uri.parse("custom://" + System.currentTimeMillis())
        alarmManager[AlarmManager.RTC_WAKEUP, timestamp!!] = pendingIntent
    }

    fun closeKeyboard(view: View, context: Context) {
        val imm: InputMethodManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboard(view: View, context: Context) {
        view.requestFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
    fun findDataById(id: Int, list: String?): String {
        var response = ""
        val listData: ArrayList<Data> = Gson()
            .fromJson<ArrayList<Data>>(
                list,
                object :
                    TypeToken<List<Data?>?>() {}.type
            )
        for (data in listData) {
            if (data.id  == id) {
                response = data.name
                break
            }
        }
        return response
    }

    fun findCreditsById(id: Int, list: String?): Int {
        var response = 0
        val listData: ArrayList<Data> = Gson()
            .fromJson<ArrayList<Data>>(
                list,
                object :
                    TypeToken<List<Data?>?>() {}.type
            )
        for (data in listData) {
            if (data.id == id) {
                response = data.credits_number
                break
            }
        }
        return response
    }

}