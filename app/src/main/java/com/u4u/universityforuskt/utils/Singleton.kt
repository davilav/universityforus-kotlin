package com.u4u.universityforuskt.utils

import com.u4u.universityforuskt.data.models.Data

object Singleton {
    var isOnline = false
    var career: Int = 0
    var career_name:String = ""
    var credits_semester: Int = 0
    var events: List<Data>? = null
    lateinit var email: String
    var faculty: Int = 0
    var first_login: Int = 0
    lateinit var first_name: String
    lateinit var last_login: String
    lateinit var last_name: String
    var new_student: Int = 0
    lateinit var token: String
    var university: Int = 0
    var user_id: Int = 0
    lateinit var username: String
}