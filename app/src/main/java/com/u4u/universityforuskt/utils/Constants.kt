package com.u4u.universityforuskt.utils

object Constants {
    const val SPLASH_TIME_OUT = 3000
    const val SNACK_TIME_OUT = 3250
    const val KEY_UNIVERSITY = "key_university"
    const val KEY_CAREER = "key_career"
    const val KEY_FACULTY = "key_faculty"
    const val DATABASE_NAME = "u4u_db"
    const val REACHABILITY_SERVER = "https://www.google.com"
}