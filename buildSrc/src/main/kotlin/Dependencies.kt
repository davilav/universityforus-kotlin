object BuildPlugins {

    object Versions {
        const val kotlin_version = "1.3.72"
        const val gradle_version = "4.0.0"
        const val google_services_version = "4.3.3"
        const val crashlytics_gradle_version = "2.1.1"
    }

    const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.gradle_version}"
    const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_version}"
    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin_version}"
    const val googleServices = "com.google.gms:google-services:${Versions.google_services_version}"
    const val crashlyticsGradlePlugin = "com.google.firebase:firebase-crashlytics-gradle:${Versions.crashlytics_gradle_version}"
    const val androidApplication = "com.android.application"
    const val kotlinAndroid = "kotlin-android"
    const val kotlinAndroidExtensions = "kotlin-android-extensions"
}

object AndroidSdk {
    const val compileSdk = 29
    const val buildTools = "29.0.3"
    const val minSdk= 21
    const val targetSdk = 29
}

object SupportLibraries {
    private object Versions {
        const val appcompat_version = "1.1.0"
        const val corektx_version = "1.2.0"
        const val legacy_version = "1.0.0"
    }

    const val appCompat = "androidx.appcompat:appcompat:${Versions.appcompat_version}"
    const val ktxCore = "androidx.core:core-ktx:${Versions.corektx_version}"
    const val legacy = "androidx.legacy:legacy-support-v4:${Versions.legacy_version}"
}

object UILibraries {
    private object Versions {
        const val constraintLayout_version = "2.0.0-beta4"
        const val recyclerview_version = "1.1.0"
        const val cardview_version = "1.0.0"
        const val material_version = "1.0.0"
        const val toasty_version = "1.4.2"
    }

    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout_version}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerview_version}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardview_version}"
    const val materialDesign = "com.google.android.material:material:${Versions.material_version}"
    const val expandable = "com.github.aakira:expandable-layout:1.6.0@aar"
    const val toasty = "com.github.GrenderG:Toasty:${Versions.toasty_version}"
}

object ArchComponentsLibraries {
    private object Versions {
        const val lifecycle_version = "2.2.0"
        const val navigation_version = "2.3.0"
        const val room_version = "2.2.5"
        const val palette_version = "1.0.0"
        const val work_manager_version = "2.3.4"
    }

    const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle_version}"
    const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle_version}"
    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation_version}"
    const val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation_version}"
    const val palette = "androidx.palette:palette:${Versions.palette_version}"
    const val workManager = "androidx.work:work-runtime:${Versions.work_manager_version}"
    const val roomRuntime =  "androidx.room:room-runtime:${Versions.room_version}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.room_version}"
    const val kts = "androidx.room:room-ktx:${Versions.room_version}"
}

object NetworkLibraries {
    private object Versions {
        const val retrofit_version = "2.9.0"
        const val okhttp_version = "4.7.2"
        const val gson_version = "2.8.5"
        const val debugDatabase_version = "1.0.6"
    }

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit_version}"
    const val retrofitGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit_version}"
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp_version}"
    const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp_version}"
    const val gson = "com.google.code.gson:gson:${Versions.gson_version}"
    const val debugDatabase = "com.amitshekhar.android:debug-db:${Versions.debugDatabase_version}"
}

object DILibraries {
    private object Versions {
        const val koin_version = "2.1.5"
    }

    const val koinScope = "org.koin:koin-androidx-scope:${Versions.koin_version}"
    const val koinViewmodel = "org.koin:koin-androidx-viewmodel:${Versions.koin_version}"
    const val koinFragment = "org.koin:koin-androidx-fragment:${Versions.koin_version}"

}

object UtilsLibraries {
    private object Versions {
        const val android_week_version = "4.1.7"
        const val picasso_version = "2.5.2"
        const val picasso_downloader_version = "1.1.0"
        const val leak_canary_version = "2.3"
        const val floating_button_version ="1.10.1"
        const val gif_drawable_version = "1.2.19"
    }

    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso_version}"
    const val androidWeek = "com.github.neolink557.Android-Week-View:core:4.1.7"
    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leak_canary_version}"
    const val floatingButton = "com.getbase:floatingactionbutton:${Versions.floating_button_version}"
    const val picassoDownloader = "com.jakewharton.picasso:picasso2-okhttp3-downloader:${Versions.picasso_downloader_version}"
    const val gifDrawable = "pl.droidsonroids.gif:android-gif-drawable:${Versions.gif_drawable_version}"
}

object FirebaseLibraries {
    object Versions {
        const val firebase_analytics_version = "17.4.3"
        const val firebase_messaging_version = "20.2.0"
        const val firebase_iid_version = "20.2.0"
        const val firebase_crashlytics_version = "17.0.1"
    }

    const val firebaseAnalytics = "com.google.firebase:firebase-analytics:${Versions.firebase_analytics_version}"
    const val firebaseMessaging = "com.google.firebase:firebase-messaging:${Versions.firebase_messaging_version}"
    const val firebaseIid = "com.google.firebase:firebase-iid:${Versions.firebase_iid_version}"
    const val firebaseCrashlytics = "com.google.firebase:firebase-crashlytics:${Versions.firebase_crashlytics_version}"
}

object AnalysisCodeLibraries {
    object AnalysisCodeVersions {
        const val ktlint_gradle_version = "8.2.0"
        const val ktlint_version = "0.36.0"
    }

    const val ktlinGradle = "org.jlleitschuh.gradle.ktlint"
    const val ktlint = "org.jlleitschuh.gradle.ktlint"
}

object TestLibraries {
    private object Versions {
        const val junit4_version = "4.13"
        const val extJunit_version = "1.1.1"
        const val espresso_version = "3.2.0"
    }

    const val junit4 = "junit:junit:${Versions.junit4_version}"
    const val extJunit = "androidx.test.ext:junit:${Versions.extJunit_version}"
    const val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso_version}"
}
